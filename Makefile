BASE=$(HOME)/EMoffice
#MUMPSDIR=$(BASE)/MUMPS/MUMPS_5.0.2
MUMPSDIR=$(BASE)/MUMPS/MUMPS_5.1.1

#MUMPS_SEQ=-L$(MUMPSDIR)/libseq -lmpiseq
LORDERINGS=-L$(MUMPSDIR)/pord/lib -lpord -lmetis

INTEL_MPI_INCLUDE=-I/opt/intel/compilers_and_libraries_2017.4.196/linux/mpi/intel64/include
#because MKLCSRPencilFD has parallel MUMPS "mpi.h" 

CC    = icc 
MPICC = mpiicpc

CCOPTS    = -I${MKLROOT}/include  -I$(MUMPSDIR)/include -Iinclude -fPIC  ${INTEL_MPI_INCLUDE}
LINK      = -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_core -lmkl_sequential -lpthread -ldl -L$(MUMPSDIR)/lib -ldmumps -lzmumps -lmumps_common -lfftw3 ${LORDERINGS}  $(LIBLAPACK) $(LIBMKLTHREAD) $(LIBMKLCORE) -lgfortran

CCOPTS-MT = -I${MKLROOT}/include -I$(MUMPSDIR)/include -Iinclude -fPIC ${INTEL_MPI_INCLUDE}
LINK-MT   = -L${MKLROOT}/lib/intel64 -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -ldl -L$(MUMPSDIR)/lib -ldmumps -lmumps_common -lfftw3 ${LORDERINGS}  $(LIBLAPACK) $(LIBMKLTHREAD) $(LIBMKLCORE) -lgfortran

LIB_SEQ_NAME    = librks.so
LIB_SEQ-MT_NAME = librks-mt.so

LIB_PAR_NAME    = libprks.so
LIB_PAR-MT_NAME = libprks-mt.so

MKDIR_P = mkdir -p
.PHONY: obj_dir 

all: obj_dir seq seq-mt par par-mt

obj_dir:
	${MKDIR_P} obj/

SEQ = obj/BlasWrapper.o \
	obj/LapackWrapper.o \
	obj/SparseBlasWrapper.o \
	obj/InnerProduct.o \
	obj/FOMPencil.o \
	obj/RAD.o \
	obj/Utils.o \
	obj/ContinuationPair.o \
	obj/SeqTemplates.o \
	obj/InternalData.o \
	obj/DiagMatrix.o \
	obj/MKLCSRPencilFD.o \
	obj/RationalArnoldi.o \


SEQ-MT = obj/BlasWrapper-mt.o \
	obj/LapackWrapper-mt.o \
	obj/SparseBlasWrapper-mt.o \
	obj/InnerProduct-mt.o \
	obj/FOMPencil-mt.o \
	obj/RAD-mt.o \
	obj/Utils-mt.o \
	obj/ContinuationPair-mt.o \
	obj/SeqTemplates-mt.o \
	obj/InternalData-mt.o \
	obj/DiagMatrix-mt.o \
	obj/MKLCSRPencilFD-mt.o \
	obj/RationalArnoldi-mt.o \



PAR = obj/pBlasWrapper.o \
	obj/pLapackWrapper.o \
	obj/pSparseBlasWrapper.o \
	obj/pInnerProduct.o \
	obj/pFOMPencil.o \
	obj/pRAD.o \
	obj/pUtils.o \
	obj/pContinuationPair.o \
	obj/pSeqTemplates.o \
	obj/pInternalData.o \
	obj/pDiagMatrix.o \
	obj/pMKLCSRPencilFD.o \
	obj/pRationalArnoldi.o \
	obj/pParTemplates.o \
	obj/pPRationalArnoldi.o\


PAR-MT = obj/pBlasWrapper-mt.o \
	obj/pLapackWrapper-mt.o \
	obj/pSparseBlasWrapper-mt.o \
	obj/pInnerProduct-mt.o \
	obj/pFOMPencil-mt.o \
	obj/pRAD-mt.o \
	obj/pUtils-mt.o \
	obj/pContinuationPair-mt.o \
	obj/pSeqTemplates-mt.o \
	obj/pInternalData-mt.o \
	obj/pDiagMatrix-mt.o \
	obj/pMKLCSRPencilFD-mt.o \
	obj/pRationalArnoldi-mt.o \
	obj/pParTemplates-mt.o \
	obj/pPRationalArnoldi-mt.o \

seq: $(SEQ)
	$(CC) -shared -Wl,-soname,$(LIB_SEQ_NAME) -o lib/$(LIB_SEQ_NAME)  $(SEQ)

seq-mt: $(SEQ-MT)
	$(CC)  -shared -Wl,-soname,$(LIB_SEQ-MT_NAME) -o lib/$(LIB_SEQ-MT_NAME)  $(SEQ-MT)

par: $(PAR)
	$(MPICC) -shared -Wl,-soname,$(LIB_PAR_NAME) -o lib/$(LIB_PAR_NAME)  $(PAR)

par-mt: $(PAR-MT)
	$(MPICC) -shared -Wl,-soname,$(LIB_PAR-MT_NAME) -o lib/$(LIB_PAR-MT_NAME)  $(PAR-MT)

obj/%.o : src/%.cpp
	$(CC) $(CCOPTS) $(LINK) -O3 -o $@ -c $<

obj/%-mt.o : src/%.cpp
	$(CC) $(CCOPTS-MT) $(LINK-MT) -O3 -o $@ -c $<

obj/p%.o : src/%.cpp
	$(MPICC) $(CCOPTS) $(LINK) -O3 -o $@ -c $<

obj/p%-mt.o : src/%.cpp
	$(MPICC) $(CCOPTS-MT) $(LINK-MT) -O3 -o $@ -c $<

clean:
	rm -f obj/*.o
