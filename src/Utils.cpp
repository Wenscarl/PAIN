// This file is part of RAIN, released under the terms of the MIT license.

#include "Utils.hpp"
namespace RKS
{
      // Prints a matrix.
  template <typename F>
  void print_matrix(char* name, int m, int n, const F* A, int lda, 
                    int precision, int w)
  {
    cout << name << std::endl;
    for(int ir = 0; ir < m; ir++)
      {
        for(int ic = 0; ic < n; ic++)
         cout << std::setw(w) << std::setprecision(precision) 
                    << std::scientific << A[ic*lda+ir];
        cout << endl;
      }
  }
  
  // Explicit instantiations.
  template void print_matrix<single>(char*, int, int, const single*, int , int, int);
  template void print_matrix<double>(char*, int, int, const double*, int , int, int);
  template void print_matrix<scmplx>(char*, int, int, const scmplx*, int , int, int);
  template void print_matrix<dcmplx>(char*, int, int, const dcmplx*, int , int, int);
  
} // namespace RKS
 
