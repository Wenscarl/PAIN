#include "MKLCSRPencilFD.hpp"
#include <list>
#include "Types.hpp"
#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654
using namespace std;


NOTE: This file just list the routines of mumps , not really compiled in project
namespace RKS
{
    
    template<typename F>
    struct MatEntry
    {
        int i_,j_;
        F val_;
        MatEntry(){i_=0;j_=0;val_=F(0.0);}
    };

    template<typename F>
    void MKLCSRPencilFD<F>::MUMPSmemfree()
    {
        if(Amumps!=NULL) free(Amumps); Amumps = NULL; 
        if(Ar!=NULL) free(Ar); Ar = NULL; 
        if(Ac!=NULL) free(Ac); Ac = NULL;   
        if(B!=NULL)  free(B); B = NULL;   
    }

    template<typename F>
    void MKLCSRPencilFD<F>::MUMPSmemloc()
    {
        umfn = N;
        umfnz = nnza(); // since pencil b is a identity ,nnzc = nnza
        Amumps = (DMUMPS_REAL*) malloc (umfnz*sizeof(DMUMPS_REAL));
        Ar = (int*) malloc (umfnz*sizeof(int));  if(Ar == NULL) EXIT_FL; 
        Ac = (int*) malloc (umfnz*sizeof(int));  if(Ac == NULL) EXIT_FL;  
        B  = (DMUMPS_REAL*) malloc (umfn*sizeof(DMUMPS_REAL));
    }


    template<typename F>
    void MKLCSRPencilFD<F>::runMumps(F mu, F nu, F* in, F *out)
    {
        bool isFactorized = true;
        //////////////////////////////////
    //     mumps routines
        
        // Form C := nu*A - mu*B.
        if(mu != _mu || nu != _nu)
        { 
            _mu = mu;
            _nu = nu;

            form_c(_mu, _nu);
            isFactorized = false;
        }
        
        // MUMPS factorization
        if(!isFactorized)
        {
            factorMumps();   //factorize updated matrix c
            isFactorized = true;
        }
        
        solveMumps(in, out);
        return;        
    }

    template<typename F>
    void MKLCSRPencilFD<F>::solveMumps(F *in, F* out)
    {
        double *null = (double *) NULL ;
        int i ;

        for (i = 0; i < umfn; i++) {
            B[i] = in[i];
        }
        
        MUMPS.rhs = B;

        MUMPS.job=3;
        dmumps_c(&MUMPS);
        
        for (i = 0; i < umfn; i++) {
            out[i] = B[i];
        }
        return; 
    }
    
    
    template<typename F>
    void MKLCSRPencilFD<F>::factorMumps()
    {
        int i , id, cc;	
        double cval;
        int num = 0, cnnt = 0;
        
        std::list< MatEntry<F> > entryList;
        entryList.clear();	
        MatEntry<F> tmpEntry;

        for (i = 0; i < umfn; i++) { //Deal with the symmetric part

            for (id = rowc[i]-1; id < rowc[i + 1]-1; id ++) {
                cc = colc[id]-1;
                if (cc < umfn) {
    // 				if(! (cc<dim)){
    // 					cout<<"CC:"<<cc<<"\tdim:"<<dim<<endl;
    // 					assert(false);
    // 				}
                    cval = valc[id];
    // 				Ar[num] = i+1;
    // 				Ac[num] = cc+1;
    // 				A[num].r  = cval.real()+cval2.real();
    // 				A[num].i = cval.imag()+cval2.imag();
                    tmpEntry.i_=i+1;
                    tmpEntry.j_=cc+1;
                    tmpEntry.val_=cval;
                    entryList.push_back(tmpEntry);
                    num++;	
                }
                else{
                    perror("dimension not right!");
                }
            }
        }


        for (i = 0; i < umfn; i++) {
            B[i] = 1.0;
        }

//         for(std::list< MatEntry<F> >::iterator itt=entryList.begin();itt!=entryList.end();itt++){
//             Ar[cnnt]=itt->i_;
//             Ac[cnnt]=itt->j_;
//             Amumps[cnnt]=itt->val_;
//             cnnt++;
//         }
        
        cout<<" nzc = "<<num<<" umfnz= "<<umfnz<<endl;
        
        int myid, ierr;
        //     ierr = MPI_Init(&argc, &argv);
        //     ierr = MPI_Comm_rank(MPI_COMM_WORLD, &myid);
        
        MUMPS.job=JOB_INIT;
        MUMPS.par=1;
        MUMPS.sym=0;
        MUMPS.comm_fortran=USE_COMM_WORLD;
        
        dmumps_c(&MUMPS);
        
        MUMPS.n = umfn;
        MUMPS.nz = umfnz;
        MUMPS.irn = Ar;
        MUMPS.jcn = Ac;
        MUMPS.a = Amumps;
        MUMPS.rhs = B;
        
        //     id.n = n; id.nz =nz; id.irn=irn; id.jcn=jcn;
        //     id.a = a; id.rhs = rhs;
        #define ICNTL(I) icntl[(I)-1] 
        
        MUMPS.ICNTL(1)=6;
        MUMPS.ICNTL(2)=0;
        MUMPS.ICNTL(3)=0;
        MUMPS.ICNTL(4)=1;
        MUMPS.ICNTL(22)=1;
        MUMPS.ICNTL(14)=666;
        
        char MUMPName[64];
        int ifreq= 999.999;
        char *fname = "helloworld";
        sprintf(MUMPName, "%s_%d.mump", fname,ifreq);
        
        //     MUMPS.ooc_prefix = MUMPName;
        strcpy( MUMPS.ooc_prefix,MUMPName);
        
        // 
        //     char DIRName[360];
        //     sprintf(DIRName, "./", name_);
        
            char TMPName[151];

            //     TMPName = "/tmp";

            char cmdName[128];

            sprintf( cmdName,"mkdir ./MUMPS");

            system(cmdName);


            sprintf(TMPName, "./MUMPS");

            strcpy( MUMPS.ooc_tmpdir,TMPName);


        MUMPS.job=4;
        dmumps_c(&MUMPS);	
        return;
    }
      // Explicit instantiations.

  template class MKLCSRPencilFD<double>;

} // end of namespace RKS
