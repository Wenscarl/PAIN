// This file is part of RAIN, released under the terms of the MIT license.

#include "InnerProduct.hpp"

namespace RKS
{
  // Output C is an m-by-n matrix, the matrices X and Y are of 
  // size k-by-n and k-by-m, respectively.
  template <typename F>
  void InnerProduct<F>::operator()(int m, int n, int k, 
                                   const F* X, int ldx, 
                                   const F* Y, int ldy,
                                   /**/  F* C, int ldc)
  {
    if(m == 1 && n == 1)      
      *C = dot(k, X, 1, Y, 1);
    else if(n == 1)
      gemv('C', k, m, F(1.0), Y, ldy, X, 1, F(0.0), C, 1);//blas's dgemv
    else
      gemm('C', 'N', m, n, k, F(1.0), Y, ldy, X, ldx, F(0.0), C, ldc);      
  }

  template <typename F>
  PencilInnerProduct<F>::PencilInnerProduct(AbstractPencil<F>* Pencil, 
                                            F eta, F rho)
    : Pencil(Pencil), eta(eta), rho(rho), Z(NULL)
  { Z = (F*) malloc(Pencil->size()*sizeof(F)); if(Z == NULL) EXIT_FL; };

  template <typename F>
  PencilInnerProduct<F>::~PencilInnerProduct()
  { if(Z != NULL) free(Z); };

  template <typename F>
  void PencilInnerProduct<F>::operator()(int m, int n, int k, 
                                         const F* X, int ldx, 
                                         const F* Y, int ldy,
                                         /**/  F* C, int ldc)
  {
    // if(k != Pencil->size()) RKS_EXIT;
    for(int j = 0; j < n; ++j)
      {
        Pencil->multiply(eta, rho, (F*)(X + ldx*j), Z);
        InnerProduct<F>::operator()(m, 1, k, 
                                    Z, Pencil->size(), Y, ldy, 
                                    C + ldc*j, ldc);
      }      
  }

  // Explicit instantiations.
  template class InnerProduct<single>;
  template class InnerProduct<double>;
  template class InnerProduct<scmplx>;
  template class InnerProduct<dcmplx>;

  template class PencilInnerProduct<single>;
  template class PencilInnerProduct<double>;
  template class PencilInnerProduct<scmplx>;
  template class PencilInnerProduct<dcmplx>;
  
} // namespace RKS
