// This file is part of RAIN, released under the terms of the MIT license.

#include "RAD.hpp"
#include "Utils.hpp"
#include <iostream>
using namespace std;
namespace RKS
{
  // For the RAD A V K = B V H, computes the relative residual 
  // || A V K - B V H ||_F / ( ||K||_F + || H ||_F ).
  template <typename F>
  typename BaseType<F>::type rad_residual
  (AbstractPencil<F>& Pencil, 
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, int m)
  { 
    int N = Pencil.size();
    
    F* AV = (F*) malloc(N*(m+1)*sizeof(F)); if (AV == NULL ) EXIT_FL;
    F* BV = (F*) malloc(N*(m+1)*sizeof(F)); if (BV == NULL ) EXIT_FL;
    F* R  = (F*) malloc(N*m*sizeof(F));     if (R  == NULL ) EXIT_FL;
    
    for(int j = 0; j < m+1; ++j)
      {
        Pencil.multiply(F(0.0), F(1.0), V+ldv*j, AV+ldv*j);
        Pencil.multiply(F(1.0), F(0.0), V+ldv*j, BV+ldv*j);
      }
    
    gemm('N', 'N', N, m, m+1, F(1.0), AV, N, K, ldk, F(0.0), R, N);  //R = 1*AV*K; AV: N*(m+1) K:(m+1)*m ;lda , ldb , ldc : leading dimension of ABC
    gemm('N', 'N', N, m, m+1, F(1.0), BV, N, H, ldh, F(1.0), R, N);  //R = F(1.0)*BV*H+F(1.0)*R

    typename BaseType<F>::type norm_K = norm('F', m+1, m, K, ldk);
    typename BaseType<F>::type norm_H = norm('F', m+1, m, H, ldh);
    typename BaseType<F>::type out = norm('F', N, m, R, N);
    
    {
	cout<<"\n";
    F* Am = (F*) malloc((m+1)*(m+1)*sizeof(F));     if  (Am == NULL) EXIT_FL;
    bool inner_product_flag = false;
    InnerProduct<F> *ip = NULL;
    if(ip == NULL)
      { 
        inner_product_flag = true;
        ip = new InnerProduct<F>();
      }
     (*ip)(m+1,m+1,N,V,ldv,AV,N,Am,m+1);
//     for (int i=0;i<m;i++){
// 		for(int ii = 0 ; ii<m;ii++){
// 		cout<<Am[i*m+ii]<<" ";
// 	}
// 	cout<<endl;
// 	}
    print_matrix("Am", m+1, m+1, Am, m+1);
	if(Am !=NULL) free(Am);
    }
    
    if(AV != NULL) free(AV);
    if(BV != NULL) free(BV);
    if(R  != NULL) free(R);

    return out/(norm_K + norm_H);
  }

  // For the RAD A V K = B V H, computes the departure from orthonormality 
  // || I - inner_product(V, V) ||_F.
  template <typename F>
  typename BaseType<F>::type orthonormality
  (int N, int m, F* V, int ldv, 
   InnerProduct<F>* inner_product)
  { 
    bool inner_product_flag = false;

    if(inner_product == NULL)
      { 
        inner_product_flag = true;
        inner_product = new InnerProduct<F>();
      }

    F* GS = (F*) malloc((m+1)*(m+1)*sizeof(F)); if (GS == NULL ) EXIT_FL;
        
    (*inner_product)(m+1, m+1, N, V, ldv, V, ldv, GS, m+1);    
    for(int j = 0; j < m+1; ++j) GS[(m+2)*j] -= F(1.0);    
        
    typename BaseType<F>::type out = norm('F', m+1, m+1, GS, m+1);
    
    if(GS != NULL) free(GS);

    if(inner_product_flag) delete inner_product;

    return out;
  }

  // Explicit instantiations.
  template single rad_residual
  (AbstractPencil<single>&, single*, int, single*, int, single*, int, int);
  template double rad_residual
  (AbstractPencil<double>&, double*, int, double*, int, double*, int, int);
  template single rad_residual
  (AbstractPencil<scmplx>&, scmplx*, int, scmplx*, int, scmplx*, int, int);
  template double rad_residual
  (AbstractPencil<dcmplx>&, dcmplx*, int, dcmplx*, int, dcmplx*, int, int);

  template single orthonormality
  (int, int, single*, int, InnerProduct<single>*);
  template double orthonormality
  (int, int, double*, int, InnerProduct<double>*);
  template single orthonormality
  (int, int, scmplx*, int, InnerProduct<scmplx>*);
  template double orthonormality
  (int, int, dcmplx*, int, InnerProduct<dcmplx>*);

} // namespace RKS
