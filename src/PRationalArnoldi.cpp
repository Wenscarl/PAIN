// This file is part of RAIN, released under the terms of the MIT license.

#include "PRationalArnoldi.hpp"

namespace RKS
{
  // MPI variant of the parallel rational Arnoldi algorithm.
  template <typename F>
  void 
  p_rational_arnoldi 
  (AbstractPencil<F>& pencil, 
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, 
   F* xi, int m, 
   MPI_Comm comm, 
   int* param, 
   int j, 
   InnerProduct<F>*   inner_product,
   AbstractPencil<F>* inexact_pencil,
   InternalData<F>*   data)
  {
    int N = pencil.size();
    F alpha, mu, nu, eta, rho;

    F* vt = NULL;
    F* vc = NULL;
    F* vw = NULL;

    bool inner_product_flag = false;

    int p, pinf,   rank;
    int s, ncycle, ncycleloc, jloc;

    MPI_Comm sub_comm;

    if(data != NULL) data->total_bgn();

    if(param[Default] == 0)       
        rational_arnoldi_init_param(param);

    if(MPI_Comm_size(comm, &p)    != MPI_SUCCESS) MPI_FILE_LINE;//comm is the input communicator
    if(MPI_Comm_rank(comm, &rank) != MPI_SUCCESS) MPI_FILE_LINE;

    vt = (F*) calloc(j+m+1, sizeof(F)); if (vt == NULL) EXIT_FL;
    vc = (F*) calloc(j+m+1, sizeof(F)); if (vc == NULL) EXIT_FL;
    vw = (F*) malloc(N*sizeof(F));      if (vw == NULL) EXIT_FL; //calloc = zero initialize 

    if(inexact_pencil == NULL) inexact_pencil = &pencil;
    if(inner_product  == NULL)
      { 
        inner_product_flag = true;
        inner_product = new InnerProduct<F>();
      }

    ContinuationPair<F> continuation_pair = 
      ContinuationPair<F>(&pencil, inexact_pencil, 
                          V, ldv, K, ldk, H, ldh, vw, vc,
                          param, j+m, inner_product);
  
    // Pad K and H with zeros.
    for(int ic = 0; ic < j; ++ic)
      for(int ir = j+1; ir < j+m+1; ++ir)      
        K[ic*ldk+ir] = H[ic*ldh+ir] = 0;

    for(int ic = j; ic < j+m; ++ic)
      for(int ir = 0; ir < j+m+1; ++ir)
        K[ic*ldk+ir] = H[ic*ldh+ir] = 0;

    //Breakdown did not occur (yet).
    param[BreakdownIter] = -1;
    
    // Normalise starting vector.
    if(j == 0)
      {
        if(data != NULL) data->partial_bgn(TimeOrth, j + rank);

        (*inner_product)(1, 1, N, V, N, V, N, &alpha, 1);
        alpha = sqrt(alpha);
        scal(N, F(1.0)/alpha, V, 1);
        
        if(data != NULL) data->set_column_r(&alpha,    1);
        if(data != NULL) data->partial_end(TimeOrth, j + rank);
      }
    
    ncycle = ceil(((double)m)/p);
    pinf   = m - (ncycle-1)*p;
    MPI_Comm_split(comm, (int)(rank < pinf), rank, &sub_comm);
    
    /// ian added a self communicator for MUMPS factorization
//     MPI_Comm self_comm;
//     MPI_Comm_split(comm, );
    ///

    ncycleloc = rank < pinf ? ncycle : ncycle - 1;

    for(int i = 0; i < ncycleloc; i++)
      {
        s    = i*p + j;
        jloc = s + rank;
        pinf = i == ncycle-1 ? (m - i*p) : p;
        
        if(data != NULL) data->partial_bgn(TimeContPair, jloc);
        
        // Get continuation pair (eta/rho, vt) of order s+1, and mu/nu = xi.
        continuation_pair(s+1, xi[i*p + rank], &mu, &nu, &eta, &rho, vt);
        
        if(data != NULL) data->partial_end(TimeContPair, jloc);
        if(data != NULL) data->partial_bgn(TimeVt,       jloc);
        if(data != NULL) data->set_column_t(vt, jloc+1);
        if(data != NULL) data->set_moebius(eta, rho, mu, nu, jloc);
        
        // Form vw = V(:, 0:s)*vt(0:s).
        gemv('N', N, s+1, F(1.0), V, ldv, vt, 1, F(0.0), vw, 1);
        
        if(data != NULL) data->partial_end(TimeVt,       jloc);
        if(data != NULL) data->partial_bgn(TimeMultiply, jloc);
        
        // Form V(:, jloc+1) = (rho*A -eta*B)*vw.
        pencil.multiply(eta, rho, vw, V+ldv*(jloc+1));
        
        if(data != NULL) data->partial_end(TimeMultiply, jloc);
        if(data != NULL) data->partial_bgn(TimeSolve,    jloc);
        
        // Copy vw = V(:, jloc+1).
        memcpy(vw, V+ldv*(jloc+1), N*sizeof(F));
        
        // Form V(:, jloc+1) = (nu*A-mu*B)w.
        pencil.solve(mu, nu, vw, V+ldv*(jloc+1));
        
        if(data != NULL) MPI_Barrier(i == ncycle-1 ? sub_comm : comm); // (i == ncycle-1) is the first group's last iteration
        if(data != NULL) data->partial_end(TimeSolve, jloc);
        if(data != NULL) data->partial_bgn(TimeOrth,  jloc);
        
        // Project V(:, jloc+1) onto V(:, 0:s).
        if(param[Orth] == CGS)
          {
            // Compute vc = V(:, 0:s)'*V(:, jloc+1).
            // Compute V(:, jloc+1) = V(:, jloc+1) - V(:, 0:s)*vc.
            (*inner_product)(s+1, 1, N, V+ldv*(jloc+1), ldv, V, ldv, vc, 1);
            gemv('N', N, s+1, F(-1.0), V, ldv,  vc, 1, 
                 /**/         F( 1.0), V+ldv*(jloc+1), 1);            
          } 
        else if(param[Orth] == MGS)
          {
            // Compute vc[imgs] = V(:, imgs)'*V(:, jloc+1), folllowed by 
            // V(:, jloc+1) = V(:, jloc+1) - V(:, imgs)*vc[imgs],
            // for imgs = 0:s.
            
            memset(vc, 0, (s+1)*sizeof(F));
            
            for(int index = 0; index < s+1; index++)
              {
                (*inner_product)(1, 1, N, V+ldv*(jloc+1), ldv, 
                                 /**/     V+ldv*index, ldv, &alpha, 1);
                axpy(N,   F(-alpha), V+ldv*index, 1, 
                     /**/ F(1.0),    V+ldv*(jloc+1), 1);
                vc[index] += alpha;
              }
          }
        else EXIT_STR("Incorrect value for Orth.");        
        
        if(data != NULL) data->partial_end(TimeOrth,     jloc);
        if(data != NULL) data->partial_bgn(TimeOrthSync, jloc);
              
        MPI_Barrier(i == ncycle-1 ? sub_comm : comm);//comm usually MPI_COMM_WORLD

        for(int k = 0; k < pinf; ++k)
          {
            if(rank == k)
              {         
                // Re-orthogonalization. TODO: Do this in parallel.
                if(param[Reorth] == 1)
                {
                  if(param[Orth] == CGS)
                    {
                      (*inner_product)(jloc+1, 1, N, V+ldv*(jloc+1), ldv, V, ldv, 
                                       K+ldk*jloc, 1);
                      gemv('N', N, jloc+1, F(-1.0), V, ldv, K+ldk*jloc, 1, 
                           /**/         F( 1.0), V+ldv*(jloc+1), 1);
                      axpy(jloc+1, F(1.0), K+ldk*jloc, 1, F(1.0), vc, 1);
                    }
                  else if(param[Orth] == MGS)
                    {
                      for(int index = 0; index < jloc+1; index++)
                        {
                          (*inner_product)(1, 1, N, V+ldv*(jloc+1), ldv, 
                                           /**/     V+ldv*index, ldv, &alpha, 1);
                          axpy(N,   F(-alpha), V+ldv*index, 1, 
                               /**/ F(1.0),    V+ldv*(jloc+1), 1);
                          vc[index] += alpha;
                        }
                    }
                  else EXIT_STR("Incorrect value for Orth.");
               }
                
                // Normalisation: compute norm.
                (*inner_product)(1, 1, N, V+ldv*(jloc+1), 
                                 N, V+ldv*(jloc+1), N, vc+jloc+1, 1);
                vc[jloc+1] = sqrt(vc[jloc+1]);
                // Normalisation: rescale the vector.
                scal(N, F(1.0)/vc[jloc+1], V+ldv*(jloc+1), 1);
              }
            
            MPI_Bcast(V+ldv*(s+k+1), N, MPI_GetDataType<F>(), k, 
                      i == ncycle-1 ? sub_comm : comm);

            if(rank == k)
              {
                if(data != NULL) data->set_column_r(vc, jloc+2);

                // Set up the reduced pencil.
                memcpy(K+ldk*jloc, vc, (jloc+2)*sizeof(F));
                memcpy(H+ldk*jloc, vc, (jloc+2)*sizeof(F));
                
                axpy(jloc+2, -rho, vt, 1, nu, K+ldk*jloc, 1);
                axpy(jloc+2, -eta, vt, 1, mu, H+ldk*jloc, 1);
              }
            else if(rank > k && rank < pinf)
              {
                // Proceed with orthogonalization.
                int index = s+k+1;
                vc[index] = F(0.0);

                (*inner_product)(1, 1, N, V+ldv*(jloc+1), ldv, 
                                 /**/     V+ldv*index, ldv, &alpha, 1);
                axpy(N,   F(-alpha), V+ldv*index, 1, 
                     /**/ F(1.0),    V+ldv*(jloc+1), 1);
                vc[index] += alpha;
              }
            
            MPI_Bcast(K+ldk*(s+k), s+k+2, MPI_GetDataType<F>(), k, 
                      i == ncycle-1 ? sub_comm : comm);
            MPI_Bcast(H+ldh*(s+k), s+k+2, MPI_GetDataType<F>(), k, 
                      i == ncycle-1 ? sub_comm : comm);
          } // for(int k = 0; k < pinf; ++k)

        if(data != NULL) data->partial_end(TimeOrthSync, jloc);
        
      } // for(int i = 0; i < ncycle; i++)

    if(vc != NULL) free(vc);
    if(vt != NULL) free(vt);
    if(vw != NULL) free(vw);

    MPI_Comm_free(&sub_comm);

    if(inner_product_flag == true) delete inner_product;
 
    if(data != NULL) data->total_end();

    return;
  } 

  // Explicit instantiations.
  template void p_rational_arnoldi 
  (AbstractPencil<single>&, 
   single*, int, single*, int, single*, int, 
   single*, int, MPI_Comm, int*, int, InnerProduct<single>*,
   AbstractPencil<single>*, InternalData<single>*);

  template void p_rational_arnoldi 
  (AbstractPencil<double>&, 
   double*, int, double*, int, double*, int, 
   double*, int, MPI_Comm, int*, int, InnerProduct<double>*, 
   AbstractPencil<double>*, InternalData<double>*);

  template void p_rational_arnoldi 
  (AbstractPencil<scmplx>&, 
   scmplx*, int, scmplx*, int, scmplx*, int, 
   scmplx*, int, MPI_Comm, int*, int, InnerProduct<scmplx>*, 
   AbstractPencil<scmplx>*, InternalData<scmplx>*);

  template void p_rational_arnoldi 
  (AbstractPencil<dcmplx>&, 
   dcmplx*, int, dcmplx*, int, dcmplx*, int, 
   dcmplx*, int, MPI_Comm, int*, int, InnerProduct<dcmplx>*, 
   AbstractPencil<dcmplx>*, InternalData<dcmplx>*);
  
} // namespace RKS
