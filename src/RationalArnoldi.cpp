// This file is part of RAIN, released under the terms of the MIT license.

#include "RationalArnoldi.hpp"

namespace RKS
{
  // Sequntial variant of the rational Arnoldi algorithm.
  template <typename F>
  void 
  rational_arnoldi 
  (AbstractPencil<F>& pencil, 
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, 
   F* xi, int m, 
   int* param, 
   int j, 
   InnerProduct<F>*   inner_product,
   AbstractPencil<F>* inexact_pencil, 
   InternalData<F>*   data)
  {
    int N = pencil.size();
    F alpha, mu, nu, eta, rho;

    F* vt = NULL;
    F* vc = NULL;
    F* vw = NULL;
   
    bool inner_product_flag = false;

    typename BaseType<F>::type breakdown_norm;
    typename BaseType<F>::type breakdown_tol;
    bool breakdown_flag = false;

    if(data != NULL) data->total_bgn();

    if(param[Default] == 0)       
        rational_arnoldi_init_param(param);     

    vt = (F*) calloc(j+m+1, sizeof(F)); if (vt == NULL) EXIT_FL;
    vc = (F*) calloc(j+m+1, sizeof(F)); if (vc == NULL) EXIT_FL;
    vw = (F*) malloc(N*sizeof(F));      if (vw == NULL) EXIT_FL;

    if(inexact_pencil == NULL) inexact_pencil = &pencil;
    if(inner_product  == NULL)
      { 
        inner_product_flag = true;
        inner_product = new InnerProduct<F>();
      }

    if(param[BreakdownTol] < 0)
      {       
        breakdown_tol = 
          (typename BaseType<F>::type)pow(10.0, param[BreakdownTol]);
      }

    ContinuationPair<F> continuation_pair = 
      ContinuationPair<F>(&pencil, inexact_pencil,
                          V, ldv, K, ldk, H, ldh, vw, vc,
                          param, j+m, inner_product);

    // Pad K and H with zeros.
    for(int ic = 0; ic < j; ++ic)
      for(int ir = j+1; ir < j+m+1; ++ir)      
        K[ic*ldk+ir] = H[ic*ldh+ir] = 0;

    for(int ic = j; ic < j+m; ++ic)
      for(int ir = 0; ir < j+m+1; ++ir)
        K[ic*ldk+ir] = H[ic*ldh+ir] = 0;
    
    //Breakdown did not occur (yet).
    param[BreakdownIter] = -1;

    // Normalise starting vector.
    if(j == 0)
      {
        if(data != NULL) data->partial_bgn(TimeOrth, j);

        (*inner_product)(1, 1, N, V, N, V, N, &alpha, 1);
        alpha = sqrt(alpha);
        scal(N, F(1.0)/alpha, V, 1);
        
        if(data != NULL) data->set_column_r(&alpha,  1);
        if(data != NULL) data->partial_end(TimeOrth, j);
      }

    for(int i = 0; i < m; i++, j++)
      {        
        if(data != NULL) data->partial_bgn(TimeContPair, j);

        // Get continuation pair (eta/rho, vt) of order j+1, and mu/nu = xi.
        continuation_pair(j+1, xi[i], &mu, &nu, &eta, &rho, vt);

        if(data != NULL) data->partial_end(TimeContPair, j);
        if(data != NULL) data->partial_bgn(TimeVt,       j);
        if(data != NULL) data->set_column_t(vt, j+1);
        if(data != NULL) data->set_moebius(eta, rho, mu, nu, j);

        // Form vw = V(:, 0:j)*vt(0:j).
        if(param[RKS::Strategy] == RKS::Last)
          // Pointing with vw to V+ldv*j not god as multiply not guaranteed to
          // leave the input vector unaltered.
          memcpy(vw, V+ldv*j, N*sizeof(F)); 
        else
          gemv('N', N, j+1, F(1.0), V, ldv, vt, 1, F(0.0), vw, 1);

        if(data != NULL) data->partial_end(TimeVt,       j);
        if(data != NULL) data->partial_bgn(TimeMultiply, j);

        // Form V(:, j+1) = (rho*A -eta*B)*vw.
        pencil.multiply(eta, rho, vw, V+ldv*(j+1));

        if(data != NULL) data->partial_end(TimeMultiply, j);
        if(data != NULL) data->partial_bgn(TimeSolve,   j);

        // Copy vw = V(:, j+1).
        memcpy(vw, V+ldv*(j+1), N*sizeof(F));

        // Form V(:, j+1) = (nu*A-mu*B)w.
        
        pencil.solve(mu, nu, vw, V+ldv*(j+1));

        if(data != NULL) data->partial_end(TimeSolve, j);
        if(data != NULL) data->partial_bgn(TimeOrth,  j);

        // Project V(:, j+1) onto V(:, 0:j).
        if(param[Orth] == CGS)
          {
            // Compute vc = V(:, 0:j)'*V(:, j+1).
            // Compute V(:, j+1) = V(:, j+1) - V(:, 0:j)*vc.
            (*inner_product)(j+1, 1, N, V+ldv*(j+1), ldv, V, ldv, vc, 1);
            gemv('N', N, j+1, F(-1.0), V, ldv,  vc, 1, 
                 /**/         F( 1.0), V+ldv*(j+1), 1);
          
            if(param[Reorth] == 1)
              {
                (*inner_product)(j+1,1,N, V+ldv*(j+1),ldv, V,ldv, K+ldk*j, 1);
                gemv('N', N, j+1, F(-1.0), V, ldv, K+ldk*j, 1, 
                     /**/         F( 1.0), V+ldv*(j+1), 1);
                axpy(j+1, F(1.0), K+ldk*j, 1, F(1.0), vc, 1);
              }
          } 
        else if(param[Orth] == MGS)
          {
            // Compute vc[imgs] = V(:, imgs)'*V(:, j+1), folllowed by 
            // V(:, j+1) = V(:, j+1) - V(:, imgs)*vc[imgs], for imgs = 0:j.

            memset(vc, 0, (j+1)*sizeof(F));

            for(int ireorth = 0; ireorth <= param[Reorth]; ++ireorth)
              for(int iter = 0; iter < j+1; iter++)
                {
                  (*inner_product)(1, 1, N, V+ldv*(j+1), ldv, 
                                   /**/     V+ldv*iter,  ldv, &alpha, 1);
                  axpy(N,   F(-alpha), V+ldv*iter,  1, 
                       /**/ F(1.0),    V+ldv*(j+1), 1);
                  vc[iter] += alpha;
                }
          }
        else EXIT_STR("Incorrect value for Orth.");

        // Normalisation & Breakdown check.
        (*inner_product)(1, 1, N, V+ldv*(j+1), N, V+ldv*(j+1), N, vc+j+1, 1);
        vc[j+1] = sqrt(vc[j+1]);

        if(param[BreakdownTol] < 0)
          {
            breakdown_norm = norm('F', j+1, 1, vc, j+1);
            if(absolute(vc[j+1]) < breakdown_tol*breakdown_norm)
              breakdown_flag = true;
          }

        if(breakdown_flag != true) scal(N, F(1.0)/vc[j+1], V+ldv*(j+1), 1);

        if(data != NULL) data->set_column_r(vc, j+2);
        if(data != NULL) data->partial_end(TimeOrth, j);

        // Set up the reduced pencil.
        memcpy(K+ldk*j, vc, (j+2)*sizeof(F));
        memcpy(H+ldk*j, vc, (j+2)*sizeof(F));
        
        axpy(j+2, -rho, vt, 1, nu, K+ldk*j, 1);
        axpy(j+2, -eta, vt, 1, mu, H+ldk*j, 1);

        if(breakdown_flag)
          {
            param[BreakdownIter] = j;
            break;
          }
      } 

    if(vc != NULL) free(vc);
    if(vt != NULL) free(vt);
    if(vw != NULL) free(vw);

    if(inner_product_flag == true) delete inner_product;

    if(data != NULL) data->total_end();
    
    return;
  } 

  void rational_arnoldi_init_param(int* param)
  {
    param[RKS::Strategy]        = RKS::Ruhe;
    param[RKS::Orth]            = RKS::CGS;
    param[RKS::Reorth]          = 1;
    param[RKS::ContinuationRoot]= RKS::RootZeroInfinity;
    param[RKS::BreakdownTol]    = -14;
    param[RKS::BreakdownIter]   = -1;    

    param[RKS::NearOptStrategy] = RKS::Ruhe;
    param[RKS::NearOptOrth]     = RKS::CGS;
    param[RKS::NearOptReorth]   = 0;
    param[RKS::NearOptRoot]     = RKS::RootZeroInfinity;
  }

  void polynomial_arnoldi_init_param(int* param)
  {
    param[RKS::Strategy]         = RKS::Last;
    param[RKS::Orth]             = RKS::CGS;
    param[RKS::Reorth]           = 0;
    param[RKS::ContinuationRoot] = RKS::RootZero;
    param[RKS::BreakdownTol]     = -14;
    param[RKS::BreakdownIter]    = -1;    
  }

  // Explicit instantiations.
  template void rational_arnoldi 
  (AbstractPencil<single>&, 
   single*, int, single*, int, single*, int, 
   single*, int, int*, int, InnerProduct<single>*, 
   AbstractPencil<single>*, InternalData<single>*);

  template void rational_arnoldi 
  (AbstractPencil<double>&, 
   double*, int, double*, int, double*, int, 
   double*, int, int*, int, InnerProduct<double>*, 
   AbstractPencil<double>*, InternalData<double>*);

  template void rational_arnoldi 
  (AbstractPencil<scmplx>&, 
   scmplx*, int, scmplx*, int, scmplx*, int, 
   scmplx*, int, int*, int, InnerProduct<scmplx>*, 
   AbstractPencil<scmplx>*, InternalData<scmplx>*);

  template void rational_arnoldi 
  (AbstractPencil<dcmplx>&, 
   dcmplx*, int, dcmplx*, int, dcmplx*, int, 
   dcmplx*, int, int*, int, InnerProduct<dcmplx>*, 
   AbstractPencil<dcmplx>*, InternalData<dcmplx>*);

} // namespace RKS
