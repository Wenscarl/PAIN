// This file is part of RAIN, released under the terms of the MIT license.

#include "ParTemplates.hpp"

namespace RKS
{  
  // Return the MPI_Datatype corresponding to template type F.
  // Implemented for type F being float, double, complex<float> 
  // or complex<double>, at the end of this document.
  template<>
  MPI_Datatype MPI_GetDataType<single>(){ return MPI_FLOAT; }
  template<>
  MPI_Datatype MPI_GetDataType<double>(){ return MPI_DOUBLE; }
  template<>
  MPI_Datatype MPI_GetDataType<scmplx>(){ return MPI_C_FLOAT_COMPLEX; }
  template<>
  MPI_Datatype MPI_GetDataType<dcmplx>(){ return MPI_DOUBLE_COMPLEX; }

} // namespace RKS
