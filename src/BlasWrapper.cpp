// This file is part of RAIN, released under the terms of the MIT license.

#include "BlasWrapper.hpp"

namespace RKS
{     
  ////////////
  // BLAS 1 //
  ////////////

  // Computes out = y'*x, for vectors of length n.
  template<> single dot
  (MKL_INT n, const single* x, MKL_INT incx, const single* y, MKL_INT incy)
  { return sdot(&n, x, &incx, y, &incy); }
  
  template<> double dot
  (MKL_INT n, const double* x, MKL_INT incx, const double* y, MKL_INT incy)
  { return ddot(&n, x, &incx, y, &incy); }
  
  template<> scmplx dot
  (MKL_INT n, const scmplx* x, MKL_INT incx, const scmplx* y, MKL_INT incy)
  { scmplx out; cdotc(&out, &n, y, &incy, x, &incx); return out; }
  
  template<> dcmplx dot
  (MKL_INT n, const dcmplx* x, MKL_INT incx, const dcmplx* y, MKL_INT incy)
  { dcmplx out; zdotc(&out, &n, y, &incy, x, &incx); return out; }
    
  // Computes x = alpha*x, for a vectors x of length n.
  template<> void scal
  (MKL_INT n, single alpha, single* x, MKL_INT incx)
  { sscal(&n, &alpha, x, &incx); }

  template<> void scal
  (MKL_INT n, double alpha, double* x, MKL_INT incx)
  { dscal(&n, &alpha, x, &incx); }

  template<> void scal
  (MKL_INT n, scmplx alpha, scmplx* x, MKL_INT incx)
  { cscal(&n, &alpha, x, &incx); }

  template<> void scal
  (MKL_INT n, dcmplx alpha, dcmplx* x, MKL_INT incx)
  { zscal(&n, &alpha, x, &incx); }

  // Computes y = a*x + b*y. (The standard Axpy would be y := a*x + y.)
  template<> void axpy
  (MKL_INT n, single alpha, const single* x, MKL_INT incx, 
   /**/       single beta,        single* y, MKL_INT incy)
  { scal(n, beta, y, incy);
    saxpy(&n, &alpha, x, &incx, y, &incy); }
  
  template<> void axpy
  (MKL_INT n, double alpha, const double* x, MKL_INT incx, 
   /**/       double beta,        double* y, MKL_INT incy)
  { scal(n, beta, y, incy);
    daxpy(&n, &alpha, x, &incx, y, &incy); }
  
  template<> void axpy
  (MKL_INT n, scmplx alpha, const scmplx* x, MKL_INT incx, 
   /**/       scmplx beta,        scmplx* y, MKL_INT incy)
  { scal(n, beta, y, incy);
    caxpy(&n, &alpha, x, &incx, y, &incy); }
  
  template<> void axpy
  (MKL_INT n, dcmplx alpha, const dcmplx* x, MKL_INT incx, 
   /**/       dcmplx beta,        dcmplx* y, MKL_INT incy)
  { scal(n, beta, y, incy);
    zaxpy(&n, &alpha, x, &incx, y, &incy); }

  ////////////
  // BLAS 2 //
  ////////////

  // Computes y := alpha*A*x + beta*y.
  template<> void gemv
  (char trans, MKL_INT m, MKL_INT n,
   single alpha, const single* A, MKL_INT lda,
   /**/          const single* x, MKL_INT incx, 
   single beta,        single* y, MKL_INT incy)
  { sgemv(&trans, &m, &n, &alpha, A, &lda, 
          x, &incx, &beta,  y, &incy); }
  
  template<> void gemv
  (char trans, MKL_INT m, MKL_INT n,
   double alpha, const double* A, MKL_INT lda,
   /**/          const double* x, MKL_INT incx, 
   double beta,        double* y, MKL_INT incy)
  { dgemv(&trans, &m, &n, &alpha, A, &lda, 
          x, &incx, &beta,  y, &incy); }
  
  template<> void gemv
  (char trans, MKL_INT m, MKL_INT n,
   scmplx alpha, const scmplx* A, MKL_INT lda,
   /**/          const scmplx* x, MKL_INT incx, 
   scmplx beta,        scmplx* y, MKL_INT incy)
  { cgemv(&trans, &m, &n, &alpha, A, &lda, 
          x, &incx, &beta,  y, &incy); }

  template<> void gemv
  (char trans, MKL_INT m, MKL_INT n,
   dcmplx alpha, const dcmplx* A, MKL_INT lda,
   /**/          const dcmplx* x, MKL_INT incx, 
   dcmplx beta,        dcmplx* y, MKL_INT incy)
  { zgemv(&trans, &m, &n, &alpha, A, &lda, 
          x, &incx, &beta,  y, &incy); }


  template<> void gbmv
  (char trans, MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku,
   single alpha, const single* A, MKL_INT lda, 
   /**/          const single* x, MKL_INT incx,
   single beta,        single* y, MKL_INT incy)
  { sgbmv(&trans, &m, &n, &kl, &ku, &alpha, A, &lda, x, &incx, 
          /**/                      &beta,  y, &incy); }

  template<> void gbmv
  (char trans, MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku,
   double alpha, const double* A, MKL_INT lda, 
   /**/          const double* x, MKL_INT incx,
   double beta,        double* y, MKL_INT incy)
  { dgbmv(&trans, &m, &n, &kl, &ku, &alpha, A, &lda, x, &incx, 
          /**/                      &beta,  y, &incy); }

  template<> void gbmv
  (char trans, MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku,
   scmplx alpha, const scmplx* A, MKL_INT lda, 
   /**/          const scmplx* x, MKL_INT incx,
   scmplx beta,        scmplx* y, MKL_INT incy)
  { cgbmv(&trans, &m, &n, &kl, &ku, &alpha, A, &lda, x, &incx, 
          /**/                      &beta,  y, &incy); }
  
  template<> void gbmv
  (char trans, MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku,
   dcmplx alpha, const dcmplx* A, MKL_INT lda, 
   /**/          const dcmplx* x, MKL_INT incx,
   dcmplx beta,        dcmplx* y, MKL_INT incy)
  { zgbmv(&trans, &m, &n, &kl, &ku, &alpha, A, &lda, x, &incx, 
          /**/                      &beta,  y, &incy); }
  
  template<> void hbmv
  (char uplo, MKL_INT n, MKL_INT k,  
   single alpha, const single* A, MKL_INT lda,
   /**/          const single* x, MKL_INT incx,
   single beta,        single* y, MKL_INT incy)
  { ssbmv(&uplo, &n, &k, &alpha, A, &lda, x, &incx, &beta, y, &incy); }

  template<> void hbmv
  (char uplo, MKL_INT n, MKL_INT k,  
   double alpha, const double* A, MKL_INT lda,
   /**/          const double* x, MKL_INT incx,
   double beta,        double* y, MKL_INT incy)
  { dsbmv(&uplo, &n, &k, &alpha, A, &lda, x, &incx, &beta, y, &incy); }

  template<> void hbmv
  (char uplo, MKL_INT n, MKL_INT k,  
   scmplx alpha, const scmplx* A, MKL_INT lda,
   /**/          const scmplx* x, MKL_INT incx,
   scmplx beta,        scmplx* y, MKL_INT incy)
  { chbmv(&uplo, &n, &k, &alpha, A, &lda, x, &incx, &beta, y, &incy); }

  template<> void hbmv
  (char uplo, MKL_INT n, MKL_INT k,  
   dcmplx alpha, const dcmplx* A, MKL_INT lda,
   /**/          const dcmplx* x, MKL_INT incx,
   dcmplx beta,        dcmplx* y, MKL_INT incy)
  { zhbmv(&uplo, &n, &k, &alpha, A, &lda, x, &incx, &beta, y, &incy); }

  ////////////
  // BLAS 3 //
  ////////////

  // Computes C := alpha*op(A)*op(B) + beta*C, where C is an m-by-n matrix.
  template<> void gemm
  (char transA, char transB, MKL_INT m, MKL_INT n, MKL_INT k, 
   single alpha, const single* A, MKL_INT lda,
   /**/          const single* B, MKL_INT ldb,
   single beta,        single* C, MKL_INT ldc)
  { sgemm(&transA, &transB, &m, &n, &k,
          &alpha, A, &lda, B, &ldb, &beta,  C, &ldc); }

  template<> void gemm
  (char transA, char transB, MKL_INT m, MKL_INT n, MKL_INT k,
   double alpha, const double* A, MKL_INT lda,
   /**/          const double* B, MKL_INT ldb,
   double beta,        double* C, MKL_INT ldc)
  { dgemm(&transA, &transB, &m, &n, &k,
          &alpha, A, &lda, B, &ldb, &beta,  C, &ldc); }

  template<> void gemm
  (char transA, char transB, MKL_INT m, MKL_INT n, MKL_INT k, 
   scmplx alpha, const scmplx* A, MKL_INT lda,
   /**/          const scmplx* B, MKL_INT ldb,
   scmplx beta,        scmplx* C, MKL_INT ldc)
  { cgemm(&transA, &transB, &m, &n, &k,
          &alpha, A, &lda, B, &ldb, &beta,  C, &ldc); }

  template<> void gemm
  (char transA, char transB, MKL_INT m, MKL_INT n, MKL_INT k, 
   dcmplx alpha, const dcmplx* A, MKL_INT lda,
   /**/          const dcmplx* B, MKL_INT ldb,
   dcmplx beta,        dcmplx* C, MKL_INT ldc)
  { zgemm(&transA, &transB, &m, &n, &k,
          &alpha, A, &lda, B, &ldb, &beta,  C, &ldc); }
  
} // namespace RationalArnoldi
