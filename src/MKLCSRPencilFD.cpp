// This file is part of RAIN, released under the terms of the MIT license.

#include "MKLCSRPencilFD.hpp"

#define JOB_INIT -1
#define JOB_END -2
#define USE_COMM_WORLD -987654
//TODO:
// Include DMUMPS for complex matrix
namespace RKS
{ 
    
  template<typename F>
  struct type_name
  {
    static const char* name()
    {
        static_assert(false, "You are missing a DECL_TYPE_NAME"); 
    }
  };
  
  template<> struct type_name<single> { static const char* name() {return "float";} };
  template<> struct type_name<double> { static const char* name() {return "double";} };
  template<> struct type_name<dcmplx> { static const char* name() {return "complexdouble";} };
  template<> struct type_name<scmplx> { static const char* name() {return "complexsingle";} };

template<typename F>
    struct MatEntry
    {
        int i_,j_;
        F val_;
        MatEntry(){i_=0;j_=0;val_=F(0.0);}
    };
    
  template<typename F>
  void MKLCSRPencilFD<F>::multiply(F eta, F rho, F* in, F* out)
  {
    if(rho != F(0.0))
      mkl_csrmv('N', this->size(), this->size(), rho,  this->descra, 
                this->vala, this->cola, this->rowa, &(this->rowa[1]), 
                in, F(0.0), out);    
     
    if(eta != F(0.0) && rho != F(0.0))
      mkl_csrmv('N', this->size(), this->size(), -eta,  this->descrb,
                this->valb, this->colb, this->rowb, &(this->rowb[1]), 
                in, F(1.0), out);  

    else if(eta != F(0.0) && rho == F(0.0))
      mkl_csrmv('N', this->size(), this->size(), -eta,  this->descrb,
                this->valb, this->colb, this->rowb, &(this->rowb[1]), 
                in, F(0.0), out);  
  }

  template<typename F>
  void MKLCSRPencilFD<F>::solve(F mu, F nu, F* in, F* out)
  {
      if(isMUMPS)
          runMumps(mu, nu, in, out);
      else
          pardisoSolve(mu, nu, in, out);
    return;
  }
  
  template<typename F>
  void MKLCSRPencilFD<F>::pardisoSolve(F mu, F nu, F* in, F* out)
  {
          // Form C := nu*A - mu*B.
    if(mu != _mu || nu != _nu)
      { 
        _mu = mu;
        _nu = nu;

        form_c(_mu, _nu);

        _phase = 12;
      }
  
    // Get LU factorization.
    if(_phase != 33)
      {            
        pardiso(pt, &maxfct, &mnum, &mtype, &_phase,
                &N, valc, rowc, colc, 
                perm, &nrhs, iparm, &msglvl, in, out, &error);

        if(error != 0) EXIT_INT(error);
         std::cout<<"This is "<<call_pardiso_cnt++<<" times call of pardiso Factorization!\n";
        _phase = 33;
      }

    // Perform backward/forward substitutions.
    pardiso(pt, &maxfct, &mnum, &mtype, &_phase,
            &N, valc, rowc, colc, 
            perm, &nrhs, iparm, &msglvl, in, out, &error);

    if(error != 0) EXIT_INT(error);
  
    return;
  }
  
  
  template<typename F>
  int MKLCSRPencilFD<F>::size() const {return N;}

  // The following routines provide pointer access to internal data 
  // representing A and/or B; the descriptor gets copied.
  template<typename F>
  void MKLCSRPencilFD<F>::get_a
  (F* val, int* col, int* row) const
  { val = vala; col = cola; row = rowa; }  

  template<typename F>
  void MKLCSRPencilFD<F>::get_a
  (F* val, int* col, int* row, char descr[6]) const
  { get_a(val, col, row); 
    memcpy(descr, descra, 6*sizeof(char)); }

  template<typename F>
  void MKLCSRPencilFD<F>::get_b
  (F* val, int* col, int* row) const
  { val = valb; col = colb; row = rowb; }
  
  template<typename F>
  void MKLCSRPencilFD<F>::get_b
  (F* val, int* col, int* row, char descr[6]) const
  { get_b(val, col, row); 
    memcpy(descr, descrb, 6*sizeof(char)); }

  template<typename F>
  void MKLCSRPencilFD<F>::set_a
  (F* val, int* col, int* row, char descr[6])
  { vala = val; cola = col; rowa = row;
    memcpy(descra, descr, 6*sizeof(char)); }
  
  template<typename F>
  void MKLCSRPencilFD<F>::set_b
  (F* val, int* col, int* row, char descr[6])
  { valb = val; colb = col; rowb = row;
    memcpy(descrb, descr, 6*sizeof(char)); }
  
  // Return number of elements stored in A or B, respectively.
  template<typename F>
  int MKLCSRPencilFD<F>::nnza() const 
  {return rowa != NULL ? rowa[N]-rowa[0] : -1;};

  template<typename F>
  int MKLCSRPencilFD<F>::nnzb() const 
  {return rowb != NULL ? rowb[N]-rowb[0] : -1;};

  // Allows to modify the internal parameter array for PARDISO.
  // The variable index is 1-based.
  template<typename F>
  int& MKLCSRPencilFD<F>::operator[] (int index) 
  { 
    if (index >= 1 && index <= 64) return iparm[index-1];
    else {EXIT_FL; return iparm[0];}
  }
  
  template<typename F>
  void MKLCSRPencilFD<F>::init(int _N, int _mt)
  {
    N = _N; 
    
    vala = NULL; cola = NULL; rowa = NULL;
    valb = NULL; colb = NULL; rowb = NULL;
    valc = NULL; colc = NULL; rowc = NULL; max_nnzc = 0;
    
    identity = 'n';
    
    mtype  = _mt;
    perm   = NULL;
    msglvl = 0;
    error  = 0;
    maxfct = mnum = nrhs = 1;
    for(int j = 0;  j < 64; ++j)
      pt[j] = NULL;
    
    // DMUMPS Memory
    umfn = 0;
    umfnz = 0;
    
    Ar = NULL;
    Ac = NULL;
    
    ZB  = NULL;
    ZAmumps = NULL;
    
    DB  = NULL;
    DAmumps = NULL;
    
    if(!isMUMPS){
        // Need to verify if this needs to be allocated always or not.
        perm = (int*) malloc (N*sizeof(int)); //perm is for pardiso permuation
        if(perm == NULL) EXIT_FL;  
    }
    _mu = F(0.0);
    _nu = F(0.0);
    _phase = 0;
    
    call_pardiso_cnt = 0;
    call_pardiso_cnt = 0;
      return;
  }  

  template<typename F>
  MKLCSRPencilFD<F>::MKLCSRPencilFD
  (int _N, int _mt, 
   F* val, int* col, int* row, char descr[6],bool _useDMUMPS)  
  {
    isMUMPS = (_useDMUMPS==true)?true:false;
    init(_N, _mt);
    set_a(val, col, row, descr);
 //   cout<<"now nnz = "<<nnza()<<endl;
    set_identity('b');
    if(!isMUMPS)
        pardisoinit(pt, &mtype, iparm);
     else
        MUMPSmemloc();
    allocate_c();
  };
    
  template<typename F>
  MKLCSRPencilFD<F>::MKLCSRPencilFD
  (int _N, int _mt, 
   F* val, int* col, int* row, char descr[6], 
   int _iparm[64])    
  {
    init(_N, _mt);
    set_a(val, col, row, descr);
    set_identity('b');
    memcpy(iparm, _iparm, 64*sizeof(int));
    allocate_c();
  };
  
  template<typename F>
  MKLCSRPencilFD<F>::MKLCSRPencilFD
  (F* val, int* col, int* row, char descr[6],
   int _N, int _mt)
  {
	  cout<<"call me 3"<<endl;
    init(_N, _mt);
    set_identity('a');
    set_b(val, col, row, descr);
    pardisoinit(pt, &mtype, iparm);
    allocate_c();
  };
  
  template<typename F>
  MKLCSRPencilFD<F>::MKLCSRPencilFD
  (F* val, int* col, int* row, char descr[6],
   int _N, int _mt, 
   int _iparm[64])
  {
    init(_N, _mt);
    set_identity('a');
    set_b(val, col, row, descr);
    memcpy(iparm, _iparm, 64*sizeof(int));
    allocate_c();
  };
  
  
  template<typename F>
  MKLCSRPencilFD<F>::MKLCSRPencilFD
  (int _N, int _mt,
   F* val1, int* col1, int* row1, char descr1[6],
   F* val2, int* col2, int* row2, char descr2[6])
  {
    init(_N, _mt);
    set_a(val1, col1, row1, descr1);
    set_b(val2, col2, row2, descr2);
    pardisoinit(pt, &mtype, iparm);
    allocate_c();
  };

  template<typename F>
  MKLCSRPencilFD<F>::MKLCSRPencilFD
  (int _N, int _mt,
   F* val1, int* col1, int* row1, char descr1[6],
   F* val2, int* col2, int* row2, char descr2[6],
   int _iparm[64])
  {
    init(_N, _mt);
    set_a(val1, col1, row1, descr1);
    set_b(val2, col2, row2, descr2);
    memcpy(iparm, _iparm, 64*sizeof(int));
    allocate_c();
  };

  template<typename F>
  MKLCSRPencilFD<F>::~MKLCSRPencilFD()
  { 
    int phase = -1; 

    if(identity == 'a' || identity == 'A')
      {  
        if(vala != NULL) free(vala);
        if(rowa != NULL) free(rowa);
        //if(cola != NULL) free(cola);
      }

    if(identity == 'b' || identity == 'B')
      {  
        if(valb != NULL) free(valb);
        if(rowb != NULL) free(rowb);
        //if(colb != NULL) free(colb);
      }
   
   if(!isMUMPS){
    pardiso(pt, &maxfct, &mnum, &mtype, &phase,
            &N, valc, rowc, colc, 
            perm, &nrhs, iparm, &msglvl, NULL, NULL, &error);
    if(error != 0) EXIT_INT(error);
   }
    else
        MUMPSmemfree();
        
 
    if(valc != NULL) free(valc);
    if(colc != NULL) free(colc);
    if(rowc != NULL) free(rowc);
  
    if(perm != NULL) free(perm);
  }

  // Allocated maximal possible memory needed for the CSR representation 
  // of C := nu*A - mu*B, where A nad B are given.
  template<typename F>
  void MKLCSRPencilFD<F>::allocate_c()
  {
    int ca, cb;

    rowc = (int*) calloc (N+1, sizeof(int));
    if(rowc == NULL) EXIT_FL;

    for(int ir = 0; ir < N; ++ir)
      {
        ca = rowa[ir]-1;
        cb = rowb[ir]-1;

        while(ca < rowa[ir+1]-1 && cb < rowb[ir+1]-1)
          {
            if(cola[ca] < colb[cb])
              { max_nnzc += 1; ca++; }
            else if(cola[ca] > colb[cb])
              { max_nnzc += 1; cb++; }
            else
              { max_nnzc += 1; ca++; cb++; }
          }
      
        max_nnzc += (rowa[ir+1]-1-ca + rowb[ir+1]-1-cb);
      }
  
    colc = (int*) malloc (max_nnzc*sizeof(int));
    if(colc == NULL) EXIT_FL;
  
    valc = (F*) malloc (max_nnzc*sizeof(F));
    if(valc == NULL) EXIT_FL;
    
    return;
  }

  // Forms the C := nu*A - mu*B represented in CSR format assuming all the
  // needed memory is allocated.
  template<typename F>
  void MKLCSRPencilFD<F>::form_c(F mu, F nu)
  {
    int ca, cb, cc = 0;

    bool symm =  
      mtype != RealNonSymm &&
      mtype != ComplexNonSymm;

    rowc[0] = 1;
    // Need to set valc, colc and rowc. Memory is allocated.
    for(int ir = 0; ir < N; ++ir)
      {
        rowc[ir+1] = rowc[ir];

        ca = rowa[ir]-1;
        cb = rowb[ir]-1;

        while(ca < rowa[ir+1]-1 && cb < rowb[ir+1]-1)        
          if(cola[ca] < colb[cb])             
            if(nu*vala[ca] != F(0.0) || (symm && cola[ca] == ir+1))
              {
                valc[cc] =  nu*vala[ca]; 
                colc[cc] = cola[ca]; 
                rowc[ir+1] += 1; 
                ca++; cc++; 
              }
            else
              ca++;                 
          else if(cola[ca] > colb[cb])
            if(mu*valb[cb] != F(0.0) || (symm && colb[cb] == ir+1))
              { 
                valc[cc] = -mu*valb[cb]; 
                colc[cc] = colb[cb]; 
                rowc[ir+1] += 1; 
                cb++; cc++; 
              }
            else
              cb++;
          else
            if(nu*vala[ca]-mu*valb[cb]!=F(0.0) || (symm && cola[ca]==ir+1))
              { 
                valc[cc] =  nu*vala[ca] - mu*valb[cb]; 
                colc[cc] = cola[ca]; 
                rowc[ir+1] += 1; 
                ca++; cb++; cc++; 
              }
            else
              {
                ca++; cb++;
              }        

        while(ca < rowa[ir+1]-1)
          if(nu*vala[ca] != F(0.0) || (symm && cola[ca] == ir+1))
            {
              valc[cc] =  nu*vala[ca];
              colc[cc] = cola[ca];
              rowc[ir+1] += 1;
              ca++; cc++;
            }
          else
            ca++;

        while(cb < rowb[ir+1]-1)
          if(mu*valb[cb] != F(0.0) || (symm && colb[cb] == ir+1))
            {
              valc[cc] = -mu*valb[cb];
              colc[cc] = colb[cb];
              rowc[ir+1] += 1;
              cb++; cc++;
            }
          else
            cb++;
      }

    return;
  }

  // Set A or B to identity matrix.
  template<typename F>
  void MKLCSRPencilFD<F>::set_identity(char mat)
  {
    F*   val = NULL;
    int* col = NULL;
    int* row = NULL;
    char*  descr = NULL;

    if(mat == 'a' || mat == 'A') 
      { val = vala; col = cola; row = rowa; descr = descra; };
    if(mat == 'b' || mat == 'B') 
      { val = valb; col = colb; row = rowb; descr = descrb; };

    if(val != NULL || col != NULL || row != NULL) 
      EXIT_STR("MKLCSRPencilFD<F>::set_identity failed.");

    val = (F*)       malloc (sizeof(F)*N);
    row = (int*) malloc (sizeof(int)*(N+1));
    /*col = (int*) malloc (sizeof(int)*N);*/

    if(val == NULL || /*col == NULL ||*/ row == NULL) 
      EXIT_STR("MKLCSRPencilFD<F>::set_identity failed (malloc).");

    descr = "DLUF ";
    for(int j = 0; j < N; ++j)
      {
        val[j] = F(1.0);
        row[j] = /*col[j] =*/ j+1;
      }
    row[N] = N+1;

    col = row; /* Added for the overlap. */

    if(mat == 'a' || mat == 'A') 
      { vala = val; cola = col; rowa = row; 
        memcpy(descra, descr, 6*sizeof(char)); };
    if(mat == 'b' || mat == 'B') 
      { valb = val; colb = col; rowb = row; 
        memcpy(descrb, descr, 6*sizeof(char)); }; 

    identity = mat;
  }

// DMUMPS routines
    template<typename F>
    void MKLCSRPencilFD<F>::MUMPSmemfree()
    {
        if(Ar!=NULL) free(Ar); Ar = NULL; 
        if(Ac!=NULL) free(Ac); Ac = NULL;   
        
        if(ZB!=NULL)  free(ZB); ZB = NULL;   
        if(ZAmumps!=NULL) free(ZAmumps); ZAmumps = NULL; 
        
        if(DB!=NULL)  free(DB); DB = NULL;   
        if(DAmumps!=NULL) free(DAmumps); DAmumps = NULL; 
        
        if(strcmp(type_name<F>::name(),"complexdouble") == 0)
             ZMUMPS.job = JOB_END;
        else if(strcmp(type_name<F>::name(),"double") == 0)
             DMUMPS.job = JOB_END;
         else
            cout<<"Not supported Type!\n";
    }

    template<typename F>
    void MKLCSRPencilFD<F>::MUMPSmemloc()
    {
	umfn = N;
	cout<<"Matrix Size: "<<umfn <<endl;
        
        umfnz = nnza(); // since pencil b is a identity ,nnzc = nnza
	cout<<"nnza (nnzc) = "<<umfnz <<endl;
       
        Ar = (int*) malloc (umfnz*sizeof(int));  if(Ar == NULL) EXIT_FL; 
        Ac = (int*) malloc (umfnz*sizeof(int));  if(Ac == NULL) EXIT_FL;  
        
        if(strcmp(type_name<F>::name(),"complexdouble") == 0)
        {
            ZAmumps = (ZMUMPS_COMPLEX*) malloc (umfnz*sizeof(ZMUMPS_COMPLEX)); if(ZAmumps == NULL) EXIT_FL;
            ZB  = (ZMUMPS_COMPLEX*) malloc (umfn*sizeof(ZMUMPS_COMPLEX));      if(ZB == NULL) EXIT_FL;
        }
        else if(strcmp(type_name<F>::name(),"double") == 0)
        {
            DAmumps = (DMUMPS_REAL*) malloc (umfnz*sizeof(DMUMPS_REAL)); if(DAmumps == NULL) EXIT_FL;
            DB  = (DMUMPS_REAL*) malloc (umfn*sizeof(DMUMPS_REAL));if(DB == NULL) EXIT_FL;
        }else
        {
            cout<<"Not supported Type!\n";
        }
    }

    
    template<typename F>
    void MKLCSRPencilFD<F>::runMumps(F mu, F nu, F* in, F *out)
    {
        bool isFactorized = true;
        //////////////////////////////////
    //     mumps routines
        
        // Form C := nu*A - mu*B.
        if(mu != _mu || nu != _nu)
        { 
            _mu = mu;
            _nu = nu;

            form_c(_mu, _nu);
            isFactorized = false;
        }
        
        // DMUMPS factorization
        if(!isFactorized)
        {
            factorMumps(*this);   //factorize updated matrix c
            isFactorized = true;
            cout<<"This is "<<call_mumps_cnt++<<" times call of Mumps Factorization!\n";
        }
        
        solveMumps(*this, in, out);
        return;        
    }
    
//     template<class F>
//     void solveMumps(MKLCSRPencilFD<F> &pencil,F *in, F* out){}
//         template<>
//     void solveMumps<dcmplx>(MKLCSRPencilFD<dcmplx> &pencil, dcmplx *in, dcmplx* out){}
//         template<>
//     void factorMumps<double>(MKLCSRPencilFD<double> &pencil){}
//         template<>
//     void factorMumps<dcmplx>(MKLCSRPencilFD<dcmplx> &pencil){}
    
    

    template<class F>
    void solveMumps(MKLCSRPencilFD<F> &pencil,F *in, F* out)
    {
        int i ;
        cout<<"here to solve a double task!\n";
        for (i = 0; i < pencil.umfn; i++) {
            pencil.DB[i] = in[i];
        }
        
        pencil.DMUMPS.rhs = pencil.DB;

        pencil.DMUMPS.job=3;
        dmumps_c(&(pencil.DMUMPS));
        
        for (i = 0; i < pencil.umfn; i++) {
            out[i] = pencil.DB[i];
        }
        return; 
    }
    
    template<>
    void solveMumps<dcmplx>(MKLCSRPencilFD<dcmplx> &pencil, dcmplx *in, dcmplx* out)
    {
        cout<<"here to solve a complex double task!\n";
        int i;
        for (i = 0; i < pencil.umfn; i++) {
            pencil.ZB[i].r = in[i].real();
            pencil.ZB[i].i = in[i].imag();
        }
        
        pencil.ZMUMPS.rhs = pencil.ZB;

        pencil.ZMUMPS.job=3;
        zmumps_c(&(pencil.ZMUMPS));
        
        for (i = 0; i < pencil.umfn; i++) {
            out[i].real() = pencil.ZB[i].r;
            out[i].imag() = pencil.ZB[i].i;
        }
        return; 
    }
    
    
    template<>
    void factorMumps<double>(MKLCSRPencilFD<double> &pencil)
    {
        int i , id, cc;	
        int num = 0, cnnt = 0;
        
        char MUMPName[64];
        int ifreq= 999.999;
        char *fname = "helloworld";
        sprintf(MUMPName, "%s_%d.mump", fname,ifreq);
        double cval;
         #define ICNTL(I) icntl[(I)-1] 

        for (i = 0; i < pencil.umfn; i++) { //Deal with the symmetric part
            for (id = pencil.rowc[i]-1; id < pencil.rowc[i + 1]-1; id ++) {
            cc = pencil.colc[id]-1;
            if (cc < pencil.umfn) {
                cval = pencil.valc[id];
                
                pencil.Ar[num] = i+1;
                pencil.Ac[num] = cc+1;
                pencil.DAmumps[num] = cval;
                num++;	
            }
            else{
                perror("dimension not right!");
            }
        }
        
            pencil.DB[i]  =   double(1.0);
        }
        cout<<" nzc = "<<num<<" umfnz= "<<pencil.umfnz<<endl;
        
            pencil.DMUMPS.job=JOB_INIT;
            pencil.DMUMPS.par=1;
            pencil.DMUMPS.sym=0;
//             pencil.DMUMPS.comm_fortran=USE_COMM_WORLD; // for seqential version
            pencil.DMUMPS.comm_fortran=(MUMPS_INT) MPI_Comm_c2f(MPI_COMM_SELF);
            
            dmumps_c(&pencil.DMUMPS);
            
            pencil.DMUMPS.n = pencil.umfn;
            pencil.DMUMPS.nz = pencil.umfnz;
            pencil.DMUMPS.irn = pencil.Ar;
            pencil.DMUMPS.jcn = pencil.Ac;
            pencil.DMUMPS.a = pencil.DAmumps;
            pencil.DMUMPS.rhs = pencil.DB;
            
            pencil.DMUMPS.ICNTL(1)=6;
            pencil.DMUMPS.ICNTL(2)=0;
            pencil.DMUMPS.ICNTL(3)=0;
            pencil.DMUMPS.ICNTL(4)=1;
            pencil.DMUMPS.ICNTL(22)=1;
            pencil.DMUMPS.ICNTL(14)=666;
            
            strcpy( pencil.DMUMPS.ooc_prefix,MUMPName);
            
            char TMPName[151];
            char cmdName[128];
            sprintf( cmdName,"mkdir ./MUMPS");
            system(cmdName);
            sprintf(TMPName, "./MUMPS");
            strcpy( pencil.DMUMPS.ooc_tmpdir,TMPName);
            pencil.DMUMPS.job=4;
            dmumps_c(&pencil.DMUMPS);	
            return;
    }
    
    template<>
    void factorMumps<dcmplx>(MKLCSRPencilFD<dcmplx> &pencil)
    {
        int i , id, cc;	
        int num = 0, cnnt = 0;
        char MUMPName[64];
        int ifreq= 999.999;
        char *fname = "helloworld";
        sprintf(MUMPName, "%s_%d.mump", fname,ifreq);
        dcmplx cval;
         #define ICNTL(I) icntl[(I)-1] 
            //ZDMUMPS_COMPLEX is a struct not std::complex!
        for (i = 0; i < pencil.umfn; i++) { //Deal with the symmetric part
            for (id = pencil.rowc[i]-1; id < pencil.rowc[i + 1]-1; id ++) {
                cc = pencil.colc[id]-1;
                if (cc < pencil.umfn) {
                    cval = pencil.valc[id];
                    
                    pencil.Ar[num] = i+1;
                    pencil.Ac[num] = cc+1;
                    pencil.ZAmumps[num].r = cval.real();
                    pencil.ZAmumps[num].i = cval.imag();
                    num++;	
                }
                else{
                    perror("dimension not right in factorMumps!");
                }
            }
            pencil.ZB[i].r = 1.0;
            pencil.ZB[i].i = 0.0;
        }
            
            pencil.ZMUMPS.job=JOB_INIT;
            pencil.ZMUMPS.par=1;
            pencil.ZMUMPS.sym=0;
//             pencil.ZMUMPS.comm_fortran=USE_COMM_WORLD;
            pencil.ZMUMPS.comm_fortran=(MUMPS_INT) MPI_Comm_c2f(MPI_COMM_WORLD);
            
            zmumps_c(&pencil.ZMUMPS);
            
            pencil.ZMUMPS.n = pencil.umfn;
            pencil.ZMUMPS.nz = pencil.umfnz;
            pencil.ZMUMPS.irn = pencil.Ar;
            pencil.ZMUMPS.jcn = pencil.Ac;
            pencil.ZMUMPS.a = pencil.ZAmumps;
            pencil.ZMUMPS.rhs = pencil.ZB;
         
            pencil.ZMUMPS.ICNTL(1)=6;
            pencil.ZMUMPS.ICNTL(2)=0;
            pencil.ZMUMPS.ICNTL(3)=0;
            pencil.ZMUMPS.ICNTL(4)=1;
            pencil.ZMUMPS.ICNTL(22)=1;
            pencil.ZMUMPS.ICNTL(14)=666;
            
            strcpy( pencil.ZMUMPS.ooc_prefix,MUMPName);
        
        //     char DIRName[360];
        //     sprintf(DIRName, "./", name_);
            char TMPName[151];
            char cmdName[128];
            sprintf( cmdName,"mkdir ./MUMPS");
            system(cmdName);
            sprintf(TMPName, "./MUMPS");
            strcpy( pencil.ZMUMPS.ooc_tmpdir,TMPName);
            pencil.ZMUMPS.job=4;
            zmumps_c(&pencil.ZMUMPS);	
            return;
    }
    
    
  // Explicit instantiations.
//   template class MKLCSRPencilFD<single>;
  template class MKLCSRPencilFD<double>;
//   template class MKLCSRPencilFD<scmplx>;
  template class MKLCSRPencilFD<dcmplx>;

} // namespace RKS

