// This file is part of RAIN, released under the terms of the MIT license.

#include "SeqTemplates.hpp"

namespace RKS
{  
  // Returns absolute value of z.
  template<> single absolute(single z) { return fabsf(z); }
  template<> double absolute(double z) { return fabs(z);  }
  template<> single absolute(scmplx z)
  { return sqrtf(z.real()*z.real() + z.imag()*z.imag()); }
  template<> double absolute(dcmplx z)
  { return  sqrt(z.real()*z.real() + z.imag()*z.imag()); }

  // Returns 'C' or 'T' for transposing.
  template<> char transpose<single>(){ return 'T'; }
  template<> char transpose<double>(){ return 'T'; }
  template<> char transpose<scmplx>(){ return 'C'; }
  template<> char transpose<dcmplx>(){ return 'C'; }
} // namespace RKS
 
