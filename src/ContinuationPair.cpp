// This file is part of RAIN, released under the terms of the MIT license.

#include "ContinuationPair.hpp"
using namespace std;
namespace RKS
{      
  template<typename F>
  ContinuationPair<F>::ContinuationPair
  (AbstractPencil<F>* pencil,
   AbstractPencil<F>* inexact_pencil,
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, 
   F* vw, F* vc, int _param[ArnoldiParamSize], int max_order, 
   InnerProduct<F>* inner_product) 
    : pencil(pencil), inexact_pencil(inexact_pencil),
      V(V), ldv(ldv), K(K), ldk(ldk), H(H), ldh(ldh), 
      L(NULL), _K(NULL), _H(NULL), 
      vw(vw), max_order(max_order), inner_product(inner_product)
  {
    for(int i = 0; i < ArnoldiParamSize; ++i) param[i] = _param[i];
    
    if(param[Strategy] == Ruhe)
      {
        ldl = max_order+1;
        L   = (F*) calloc (max_order*ldl, sizeof(F)); if(L == NULL) EXIT_FL;
      }
    
    if(param[Strategy] == NearOptimal)
      {
        _K = (F*) malloc (max_order*ldk*sizeof(F));
        _H = (F*) malloc (max_order*ldh*sizeof(F));
      }  
  };  
  
  template<typename F>
  ContinuationPair<F>::~ContinuationPair() 
  { 
    if( L != NULL) free( L); 
    if(_K != NULL) free(_K); 
    if(_H != NULL) free(_H); 
  }  
  
  template<typename F>
  void ContinuationPair<F>::operator()
    (int order, F xi, F* mu, F* nu, F* eta, F* rho, F* vt)
  { 
    F alpha;
    
    int N = pencil->size(), _time;

    // Set up (preliminary) values for mu, nu, eta and rho.
    *nu  = 1.0;  *mu  = xi;
    switch(param[RKS::ContinuationRoot])
      {
      case RKS::RootZero:
        *rho = F(-1.0);  *eta = F(0.0);
        if(isinf(absolute(xi))) 
          { *nu  = F( 0.0); *mu = F(-1.0); }
        break;

      case RKS::RootInfinity:
        *rho = F(0.0);  *eta = F(1.0);
        if(isinf(absolute(xi)))
          { *nu  = F( 0.0); *mu  = F(-1.0);
            *rho = F(-1.0); *eta = F( 0.0); }
        break;

      case RKS::RootZeroInfinity:
        *rho = F(-1.0);  *eta = F(0.0);
        if(isinf(absolute(xi)))
          { *nu  = F( 0.0); *mu  = F(-1.0);
            *rho = F(-1.0); *eta = F( 0.0); }
        if(absolute(xi) < 1)  
          { *rho = F(0.0); *eta = F(1.0); }
        break;

      case RKS::RootAdaptive:
      default:  
        EXIT_STR("Incorrect value for ContinuationRoot.");
      }        
        
    memset(vt, 0, order*sizeof(F));
    
    switch(param[Strategy])
      {
      case Last: 
        vt[order-1] = F(1.0);
        break;
        
      case Ruhe:
        // Form L = mu*K  - nu*H, of size order-by-(order-1).
        vt[order-1] = F(1.0);
        if (order == 1) break;
        for(int ic = 0; ic < order-1; ic++)
          {
            axpy(order,   *mu,  K+ldk*ic, 1, F(0.0), L+ldl*ic, 1);
            axpy(order, -(*nu), H+ldh*ic, 1, F(1.0), L+ldl*ic, 1);
          }      
        last_column_of_q(order, L, ldl, vt);
        break;
      
      case NearOptimal:
        std::swap(param[RKS::Strategy],         param[RKS::NearOptStrategy]);
        std::swap(param[RKS::Orth],             param[RKS::NearOptOrth]);
        std::swap(param[RKS::Reorth],           param[RKS::NearOptReorth]);
        std::swap(param[RKS::ContinuationRoot], param[RKS::NearOptRoot]);

        rational_arnoldi(*inexact_pencil, V, ldv, K, ldk, H, ldh,
                         &xi, 1, param, order-1, inner_product);
        
        std::swap(param[RKS::Strategy],         param[RKS::NearOptStrategy]);
        std::swap(param[RKS::Orth],             param[RKS::NearOptOrth]);
        std::swap(param[RKS::Reorth],           param[RKS::NearOptReorth]);
        std::swap(param[RKS::ContinuationRoot], param[RKS::NearOptRoot]);
        
        optimal_pair(order, *mu, *nu, K, _K, ldk, H, _H, ldh, eta, rho, vt);
        break;
        
      default:  
        EXIT_STR("Incorrect value for Strategy.");
      }
    
    return;
  }

  // Computes the full QR factorization of the order-by-(order-1) matrix L, 
  // and returns the last column of the Q factor via vt.
  template <typename F>
  void last_column_of_q(int order, F* L, int ldl, F* vt)
  {
    F *work, *tau, query;
    int lwork, info;
    
    tau = (F*) malloc ((order-1)*sizeof(F)); if(tau == NULL) EXIT_FL;   
    
    // Compute QR factorisation of L.
    geqrf(order, order-1, L, ldl, tau, &query, -1, &info);
    if(info != 0) EXIT_INFO("geqrf", info);
    
    lwork = (int)absolute(query);
    work  = (F*) malloc (lwork*sizeof(F)); if(work == NULL) EXIT_FL;
    
    geqrf(order, order-1, L, ldl, tau, work, lwork, &info);
    if(info != 0) EXIT_INFO("geqrf", info);
        
    // Apply Q-factor onto vt = [0, ..., 0, 1]'.
    qmqr('L', 'N', order, 1, order-1, 
         L, ldl, tau, vt, order, &query, -1, &info);
    if(info != 0) EXIT_INFO("qmqr", info);

    if(absolute(query) > lwork)
      {
        lwork = (int)absolute(query); if (work != NULL) free(work);
        work  = (F*) malloc (lwork*sizeof(F)); if(work == NULL) EXIT_FL;
      }
    
    memset(vt, 0, order*sizeof(F)); 
    vt[order-1] = F(1.0);

    qmqr('L', 'N', order, 1, order-1, 
         L, ldl, tau, vt, order, work, lwork, &info);
    if(info != 0) EXIT_INFO("qmqr", info);

    if (work != NULL) free(work);
    if (tau  != NULL) free(tau);

    return;
  }
  
  // Computes the generalized eigenvalue decomposition of the pair (H, K) of
  // size order-by-order.
  // For real-valued data the generalized eigenvalues are given by
  // (alphar + 1i*alphai, beta), while the eigenvectors are stored in X if
  // real-valued, otherwise their real and imaginary parts are stored in two
  // consecutive columns of X.
  // For complex-valued data the generalized eigenvalues are given by
  // (alphar, beta), and the corresponding eigenvectors are stored in X.
  template <typename F>
  void pencil_roots(int order, F* K, int ldk, F* H, int ldh, 
                    F* alphar, F* alphai, F* beta, F* X)
  {
    int ilo, ihi, info, lwork, irows, icols, mm;    
    F *work = NULL, *tau = NULL, query, *YNULL = NULL;
    
    typename BaseType<F>::type* bwork  = NULL;
    typename BaseType<F>::type* lscale = NULL;
    typename BaseType<F>::type* rscale = NULL;
    
    size_t allocsize = order*sizeof(typename BaseType<F>::type);
    bwork  = (typename BaseType<F>::type*) malloc (allocsize*6); 
    lscale = (typename BaseType<F>::type*) malloc (allocsize);
    rscale = (typename BaseType<F>::type*) malloc (allocsize);
    if(bwork  == NULL) EXIT_FL;
    if(lscale == NULL) EXIT_FL;
    if(rscale == NULL) EXIT_FL;

    // Balance (H, K).
    ggbal('B', order, H,ldh, K,ldk, &ilo, &ihi, lscale, rscale, bwork, &info);
    if(info != 0) EXIT_INFO("ggbal", info);

    irows = ihi   + 1 - ilo;
    icols = order + 1 - ilo;

    // Compute QR factorisation of K.
    tau = (F*) malloc (order*sizeof(F)); if(tau == NULL) EXIT_FL;

    geqrf(irows, icols, K + (ilo-1)*(ldk+1), ldk, tau, &query, -1, &info);
    if(info != 0) EXIT_INFO("geqrf", info);

    lwork = (int)absolute(query); if(work != NULL) free(work);
    work  = (F*) malloc (lwork*sizeof(F));

    geqrf(irows, icols, K + (ilo-1)*(ldk+1), ldk, tau, work, lwork, &info);
    if(info != 0) EXIT_INFO("geqrf", info);

    // Apply the Q-factor of K onto H.
    qmqr('L', transpose<F>(), irows, icols, irows, K + (ilo-1)*(ldk+1), ldk,
         tau, H + (ilo-1)*(ldh+1), ldh, &query, -1, &info);
    if(info != 0) EXIT_INFO("qmqr", info);

    if(absolute(query) > lwork)
      {       
        lwork = (int)absolute(query); if(work != NULL) free(work);
        work  = (F*) malloc (lwork*sizeof(F));
      }
    
    qmqr('L', transpose<F>(), irows, icols, irows, K + (ilo-1)*(ldk+1), ldk,
         tau, H + (ilo-1)*(ldh+1), ldh, work, lwork, &info);
    if(info != 0) EXIT_INFO("qmqr", info);

    // Reduce (H, K) to generalizes upper-Hessenberg form.
    gghrd('N', 'V', order, ilo, ihi, H, ldh, K, ldk, YNULL, 1, X, order, &info);
    if(info != 0) EXIT_INFO("gghrd", info);

    // Compute generalized eigenvalues, and Schur form of (H, K).
    hgeqz('S', 'N', 'V', order, ilo, ihi, H, ldh, K, ldk, 
          alphar, alphai, beta, YNULL, 1, X, order, &query, -1, bwork, &info);
    if(info != 0) EXIT_INFO("hgqez", info);

    if(absolute(query) > lwork)
      {
        lwork = (int)absolute(query); if(work != NULL) free(work);
        work  = (F*) malloc (lwork*sizeof(F)); if(work == NULL) EXIT_FL;
      }
    
    hgeqz('S', 'N', 'V', order, ilo, ihi, H, ldh, K, ldk, 
          alphar, alphai, beta, YNULL, 1, X, order, work, lwork, bwork, &info);
    if(info != 0) if(info != 0) EXIT_INFO("hgqez", info);
    
    if(6*order > lwork)
      {
        lwork = 6*order; if(work != NULL) free(work);
        work  = (F*) malloc (lwork*sizeof(F)); if(work == NULL) EXIT_FL;
      }
    
    // Compute right eigenvectors of (H, K).
    tgevc('R', 'B', NULL, order, H, ldh, K, ldk, YNULL, 1, X, order, 
          order, &mm, work, bwork, &info);
    if(info != 0) EXIT_INFO("tgevc", info);
  
    // Back-transform them.
    ggbak('B', 'R', order, ilo, ihi, lscale, rscale, mm, X, order, &info);
    if(info != 0) EXIT_INFO("ggbak", info);
 
    if(tau    != NULL) free(tau);
    if(work   != NULL) free(work);
    if(bwork  != NULL) free(bwork);
    if(lscale != NULL) free(lscale);
    if(rscale != NULL) free(rscale);
  }

  // Computes a (near) optimal continuation pair of given order for the pencil 
  // (H, K) of size order-by-order. The matrices H and X are overwritten. The 
  // produced continuation pair is given by (eta/rho, vt).
  template <typename F>
  void optimal_pair(int order, F mu, F nu, 
                    F* K, F* _K, int ldk, F* H, F* _H, int ldh, 
                    F* eta, F* rho, F* vt)
  {
    F *alphar = NULL, *alphai = NULL, *beta = NULL, *X = NULL;

    memcpy(_K, K, order*ldk*sizeof(F));
    memcpy(_H, H, order*ldh*sizeof(F));

    X = (F*) calloc (order*order, sizeof(F)); if(X == NULL) EXIT_FL;

    for(int j = 0; j < order; ++j) X[(order+1)*j] = F(1.0);

    beta   = (F*) malloc (order*sizeof(F));  if(beta   == NULL) EXIT_FL;
    alphar = (F*) malloc (order*sizeof(F));  if(alphar == NULL) EXIT_FL;
    alphai = (F*) malloc (order*sizeof(F));  if(alphai == NULL) EXIT_FL;    
    
    pencil_roots(order, _K, ldk, _H, ldh, alphar, alphai, beta, X);

    int minimum = 0;
    for(int j = 1; j < order; ++j)
      if(absolute(alphai[j]) == 0.0 || absolute(alphai[j]/alphar[j]) < 
         absolute(alphai[minimum]/alphar[minimum])) minimum = j;

    *eta = alphar[minimum]; *rho = beta[minimum];
    
    gemv('N', order, order,  nu, H, ldh, X + minimum*order, 1, F(0.0), vt, 1);
    gemv('N', order, order, -mu, K, ldk, X + minimum*order, 1, F(1.0), vt, 1);

    if(X      != NULL) free(X);
    if(beta   != NULL) free(beta);
    if(alphar != NULL) free(alphar);
    if(alphai != NULL) free(alphai);

    return;
  }

  // Explicit instantiations.  
  template void last_column_of_q(int, single*, int, single*);
  template void last_column_of_q(int, double*, int, double*);
  template void last_column_of_q(int, scmplx*, int, scmplx*);
  template void last_column_of_q(int, dcmplx*, int, dcmplx*);

  template void pencil_roots(int, single*, int, single*, int,
                             single*, single*, single*, single*);
  template void pencil_roots(int, double*, int, double*, int,
                             double*, double*, double*, double*);
  template void pencil_roots(int, scmplx*, int, scmplx*, int,
                             scmplx*, scmplx*, scmplx*, scmplx*);
  template void pencil_roots(int, dcmplx*, int, dcmplx*, int,
                             dcmplx*, dcmplx*, dcmplx*, dcmplx*);

  template void optimal_pair(int , single, single, single*, single*, int,
                             single*, single*, int, single*, single*, single*);
  template void optimal_pair(int , double, double, double*, double*, int,
                             double*, double*, int, double*, double*, double*);
  template void optimal_pair(int , scmplx, scmplx, scmplx*, scmplx*, int,
                             scmplx*, scmplx*, int, scmplx*, scmplx*, scmplx*);
  template void optimal_pair(int , dcmplx, dcmplx, dcmplx*, dcmplx*, int,
                             dcmplx*, dcmplx*, int, dcmplx*, dcmplx*, dcmplx*);

  template class ContinuationPair<single>;
  template class ContinuationPair<double>;
  template class ContinuationPair<scmplx>;
  template class ContinuationPair<dcmplx>;
} // namespace RKS


        /* 
        // Checking if vt is orthogonal to L, and if it is of unit norm.
        
        F* check = (F*) malloc(order*sizeof(F)); if (check == NULL) EXIT_FL;
        for(int ic = 0; ic < order-1; ic++)
        { axpy(order,   *mu,  K+ldk*ic, 1, F(0.0), L+ldl*ic, 1);
          axpy(order, -(*nu), H+ldh*ic, 1, F(1.0), L+ldl*ic, 1); }
        gemm('C', 'N', 1, order-1, order, 
             F(1.0), vt, order, L, ldl, F(0.0), check, 1);
                
        Print("(Last column)'*L", 1, order-1, check, 1);
        std::cout << "norm = " 
                  << norm('F', order, 1, vt, order) << std::endl;
        */
