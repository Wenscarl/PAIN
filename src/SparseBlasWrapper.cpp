// This file is part of RAIN, released under the terms of the MIT license.

#include "SparseBlasWrapper.hpp"

namespace RKS
{
  // Performs y:= alpha*A*x + beta*y.
  template <> void mkl_csrmv
  (char trans, int m, int k, single alpha, char matdescra[6], 
   single* val, int* col, int* ptrb, int* ptre, 
   single* x, single beta, single* y)
  { mkl_scsrmv(&trans, &m, &k, &alpha, matdescra, 
               val, col, ptrb, ptre, x, &beta, y); }
  
  template <> void mkl_csrmv
  (char trans, int m, int k, double alpha, char matdescra[6], 
   double* val, int* col, int* ptrb, int* ptre, 
   double* x, double beta, double* y)
  { mkl_dcsrmv(&trans, &m, &k, &alpha, matdescra, 
               val, col, ptrb, ptre, x, &beta, y); }
  
  template <> void mkl_csrmv
  (char trans, int m, int k, scmplx alpha, char matdescra[6], 
   scmplx* val, int* col, int* ptrb, int* ptre, 
   scmplx* x, scmplx beta, scmplx* y)
  { mkl_ccsrmv(&trans, &m, &k, &alpha, matdescra, 
               val, col, ptrb, ptre, x, &beta, y); }
  
  template <>  void mkl_csrmv
  (char trans, int m, int k, dcmplx alpha, char matdescra[6], 
   dcmplx* val, int* col, int* ptrb, int* ptre, 
   dcmplx* x, dcmplx beta, dcmplx* y)
  { mkl_zcsrmv(&trans, &m, &k, &alpha, matdescra, 
               val, col, ptrb, ptre, x, &beta, y); }
  

  // Performs y := alpha*inv(A)*x.
  template <> void mkl_csrsv
  (char trans, int m, single alpha, char matdescra[6], 
   single* val, int* col, int* ptrb, int* ptre,
   single* x, single* y)
  { mkl_scsrsv(&trans, &m, &alpha, matdescra, 
               val, col, ptrb, ptre, x, y); }
  
  template <> void mkl_csrsv
  (char trans, int m, double alpha, char matdescra[6], 
   double* val, int* col, int* ptrb, int* ptre,
   double* x, double* y)
  { mkl_dcsrsv(&trans, &m, &alpha, matdescra, 
               val, col, ptrb, ptre, x, y); }
    
  template <> void mkl_csrsv
  (char trans, int m, scmplx alpha, char matdescra[6], 
   scmplx* val, int* col, int* ptrb, int* ptre,
   scmplx* x, scmplx* y)
  { mkl_ccsrsv(&trans, &m, &alpha, matdescra, 
               val, col, ptrb, ptre, x, y); }

  template <> void mkl_csrsv
  (char trans, int m, dcmplx alpha, char matdescra[6], 
   dcmplx* val, int* col, int* ptrb, int* ptre,
   dcmplx* x, dcmplx* y)
  { mkl_zcsrsv(&trans, &m, &alpha, matdescra, 
               val, col, ptrb, ptre, x, y); }


  // Perform C := A+beta*op(B).
  template <> void mkl_csradd
  (char trans, int request, int sort, int m, int n, 
   single* a, int* ja, int* ia, single beta, 
   single* b, int* jb, int* ib, 
   single* c, int* jc, int* ic, int nzmax, int* info)
  { mkl_scsradd(&trans, &request, &sort, &m, &n, 
                a, ja, ia, &beta, b, jb, ib, c, jc, ic, &nzmax, info); }

  template <> void mkl_csradd
  (char trans, int request, int sort, int m, int n, 
   double* a, int* ja, int* ia, double beta, 
   double* b, int* jb, int* ib, 
   double* c, int* jc, int* ic, int nzmax, int* info)
  { mkl_dcsradd(&trans, &request, &sort, &m, &n, 
                a, ja, ia, &beta, b, jb, ib, c, jc, ic, &nzmax, info); }

  template <> void mkl_csradd
  (char trans, int request, int sort, int m, int n, 
   scmplx* a, int* ja, int* ia, scmplx beta, 
   scmplx* b, int* jb, int* ib, 
   scmplx* c, int* jc, int* ic, int nzmax, int* info)
  { mkl_ccsradd(&trans, &request, &sort, &m, &n, 
                a, ja, ia, &beta, b, jb, ib, c, jc, ic, &nzmax, info); }

  template <> void mkl_csradd
  (char trans, int request, int sort, int m, int n, 
   dcmplx* a, int* ja, int* ia, dcmplx beta, 
   dcmplx* b, int* jb, int* ib, 
   dcmplx* c, int* jc, int* ic, int nzmax, int* info)
  { mkl_zcsradd(&trans, &request, &sort, &m, &n, 
                a, ja, ia, &beta, b, jb, ib, c, jc, ic, &nzmax, info); }

} // namespace RKS
