// This file is part of RAIN, released under the terms of the MIT license.

#include "FOMPencil.hpp"

namespace RKS
{  
  template<typename F>
  int ShiftedPencil<F>::size() const { return Pencil->size(); }
  
  template<typename F>
  ShiftedPencil<F>::ShiftedPencil(AbstractPencil<F>* Pencil, F mu, F nu)
    : Pencil(Pencil), mu(mu), nu(nu) 
  {};
  
  template<typename F>
  void ShiftedPencil<F>::multiply(F eta, F rho, F* in, F* out)
  { 
    Pencil->multiply(rho*mu, rho*nu, in, out);
    if(eta != F(0.0))
      axpy(this->size(), -eta, in, 1, F(1.0), out, 1);
  }
  
  template<typename F>
  void ShiftedPencil<F>::solve(F mu, F nu, F* in, F* out)
  {
    if(nu != F( 0.0)) EXIT_STR("ShiftedPencil supports only infinite poles.");
    memcpy(out, in, this->size()*sizeof(F));
    if(mu != F(-1.0)) scal(this->size(), F(-1.0)/mu, out, 1);
  }
  
  template<typename F>
  int FOMPencil<F>::size() const { return Pencil->size(); };
    
  template<typename F>
  FOMPencil<F>::FOMPencil(AbstractPencil<F>* Pencil, 
			  InnerProduct<F>*   inner_product,
			  int m, int _param[ArnoldiParamSize])
    : Pencil(Pencil), inner_product(inner_product), m(m), 
      V(NULL), K(NULL), ldk(m+1), H(NULL), ldh(m+1), xi(NULL)
  {
    ldv = this->size();
    
    V  = (F*) malloc (ldv*ldk*sizeof(F)); if(V  == NULL) EXIT_FL;
    K  = (F*) malloc (m*ldk*sizeof(F));   if(K  == NULL) EXIT_FL;
    H  = (F*) malloc (m*ldh*sizeof(F));   if(H  == NULL) EXIT_FL;
    xi = (F*) malloc (m*sizeof(F));       if(xi == NULL) EXIT_FL;
    
    for(int j = 0; j < m; ++j)
      xi[j] = std::numeric_limits<F>::infinity();
    
    for(int j = 0; j < ArnoldiParamSize; ++j) 
      param[j] = _param[j];
  }
  
  template<typename F>
  FOMPencil<F>::~FOMPencil() 
  {
    if(V  != NULL) free(V);
    if(K  != NULL) free(K);
    if(H  != NULL) free(H);
    if(xi != NULL) free(xi);      
  };
  
  template<typename F>
  void FOMPencil<F>::multiply(F eta, F rho, F* in, F* out)
  { Pencil->multiply(eta, rho, in, out); }
  
  template<typename F>
  void FOMPencil<F>::solve(F mu, F nu, F* in, F* out)
  {
    F   alpha, *vb   = NULL;
    int info,  *ipiv = NULL;
    ShiftedPencil<F> shPencil(this->Pencil, mu, nu);
    
    if(nu == F(0.0)) shPencil.solve(mu, nu, in, out);      
    else
      {
        memcpy(V, in, ldv*sizeof(V));
        rational_arnoldi(shPencil, V, ldv, K, ldk, H, ldh, 
                         xi, m, param, 0, inner_product);
        
        // Compute LU of H.
        ipiv = (int*)K; // K is no longer needed.
        getrf(m, m, H, ldh, ipiv, &info);
        if(info != 0) EXIT_INFO("getrf", info);
        
        // Set vb = e_1.
        vb  = V + m*ldv; // Last column of V is no longer needed.
        memset(vb, 0, m*sizeof(F)); 
        *vb = F(1.0);
        
        // Compute vb := norm(*in)*(Hb).
        getrs('N', m, 1, H, ldh, ipiv, vb, ldv, &info);
        if(info != 0) EXIT_INFO("getrs", info);
        
        (*inner_product)(1, 1, ldv, in, ldv, in, ldv, &alpha, 1);
        scal(m, F(sqrt(alpha)), vb, 1);
        
        // Computes out = V*vb.
        gemv('N', ldv, m, F(1.0), V, ldv, vb, 1, F(0.0), out, 1);
      }
  }

  // Explicit instantiations.
  template class ShiftedPencil<single>;
  template class ShiftedPencil<double>;
  template class ShiftedPencil<scmplx>;
  template class ShiftedPencil<dcmplx>;

  template class FOMPencil<single>;
  template class FOMPencil<double>;
  template class FOMPencil<scmplx>;
  template class FOMPencil<dcmplx>;

} // namespace RationalArnoldi
