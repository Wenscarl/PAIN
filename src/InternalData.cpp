// This file is part of RAIN, released under the terms of the MIT license.

#include "InternalData.hpp"

namespace RKS
{
  template <typename F>
  InternalData<F>::InternalData(int order, double(*timer)(void))
    : order(order), ldr(order+1), R(NULL), T(NULL),
      eta(NULL), rho(NULL), mu(NULL), nu(NULL), timer(timer)
  {
    R = (F*) calloc (ldr*ldr,     sizeof(F)); if(R == NULL) EXIT_FL;
    T = (F*) calloc (order*order, sizeof(F)); if(T == NULL) EXIT_FL;
    
    eta = (F*) malloc (order*sizeof(F)); if(eta == NULL) EXIT_FL;
    rho = (F*) malloc (order*sizeof(F)); if(rho == NULL) EXIT_FL;
    mu  = (F*) malloc (order*sizeof(F)); if(mu  == NULL) EXIT_FL;
    nu  = (F*) malloc (order*sizeof(F)); if(nu  == NULL) EXIT_FL;

    total = 0.0;
    for(int j = 0; j < TimeNPar; ++j)
      {
        partial[j] = (double*) calloc(order, sizeof(double)); 
        if(partial[j] == NULL) EXIT_FL;
      }
  };

  template <typename F>
  InternalData<F>::~InternalData()
  {
    if(R   != NULL) free(R);    if(T   != NULL) free(T);
    if(mu  != NULL) free(mu);   if(nu  != NULL) free(nu);
    if(eta != NULL) free(eta);  if(rho != NULL) free(rho);
    
    for(int j = 0; j < TimeNPar; ++j)
      if(partial[j] != NULL) free(partial[j]);        
  };

  template <typename F>
  void InternalData<F>::total_bgn() { total -= timer(); }
  template <typename F>
  void InternalData<F>::total_end() { total += timer(); }

  template <typename F>
  void InternalData<F>::total_print(int precision, int w) 
  {
    std::cout << std::setw(w+1) << std::setprecision(precision+1)
              << std::scientific   << total << std::endl;
  }

  template <typename F>
  void InternalData<F>::partial_bgn(TimeTag which, int j) 
  { 
    if(j < order)
      partial[which][j] -= timer();
  }
  
  template <typename F>
  void InternalData<F>::partial_end(TimeTag which, int j) 
  { 
    if(j < order)
      partial[which][j] += timer();
  }
  
  template <typename F>
  void InternalData<F>::partial_print(int rows, int cols, 
                                      int incr,int precision,
                                      int w)
  {      
    if(order ==  0) return;
    if(cols  == -1) cols = order;
    for(int part = 0; part < rows; ++part)
      {
        for(int j = 0; j < cols; j += incr)
          std::cout << std::setw(w) << std::setprecision(precision)
                    << std::scientific << partial[part][j] << " ";
        std::cout << std::endl;
      }
    return;
  }

  // Copies vector vv of size j into the leading j rows 
  // of R's jth column. (For j = 1 into the first one.) 
  template <typename F>
  void InternalData<F>::set_column_r(F* vv, int j)
  { if(j-1 < ldr) memcpy(R + (j-1)*ldr, vv, j*sizeof(F)); }
  
  // Copies vector vv of size j into the leading j rows 
  // of T's jth column. (For j = 1 into the first one.) 
  template <typename F>
  void InternalData<F>::set_column_t(F* vv, int j)
  { if(j-1 < order) memcpy(T + (j-1)*order, vv, j*sizeof(F)); }
  
  // Copies parameters (eta/rho, mu/nu) into the (j+1)st position of
  // the corresponding vectors. (For j = 0 into the first position.)
  template <typename F>
  void InternalData<F>::set_moebius(F _eta, F _rho, F _mu, F _nu, int j)
  { if(j >= order) return;
      eta[j] = _eta; rho[j] = _rho; mu[j] = _mu; nu[j] = _nu; }
  
  // Provides copies of the relevant data. (Not timings.)
  template <typename F>
  void InternalData<F>::get_data(F* _R, F* _T, F* _eta, F* _rho, F* _mu, F* _nu)
  {
    memcpy(_R, R,     ldr*ldr*sizeof(F));
    memcpy(_T, T, order*order*sizeof(F));
    memcpy(_eta, eta,   order*sizeof(F));
    memcpy(_rho, rho,   order*sizeof(F));
    memcpy(_mu,  mu,    order*sizeof(F));
    memcpy(_nu,  nu,    order*sizeof(F));
  }

  // Explicit instantiations.
  template class InternalData<single>;
  template class InternalData<double>;
  template class InternalData<scmplx>;
  template class InternalData<dcmplx>;

} // namespace RKS
