// This file is part of RAIN, released under the terms of the MIT license.

#include "LapackWrapper.hpp"

namespace RKS
{
  // Computes the 'F', '1' or 'I' norm of an m-by-n matrix A. 
  template<> single norm
  (char norm, MKL_INT m, MKL_INT n, const single* A, MKL_INT lda)
  {
    single out, *work = NULL;
    if(norm == 'i' || norm == 'I') 
      {
        work = (single*) malloc(m*sizeof(single));
        if(work == NULL) exit(EXIT_FAILURE);
      }
    out = slange(&norm, &m, &n, A, &lda, work);
    if(work != NULL) free(work);
    return out;
  }
  
  template<> double norm
  (char norm, MKL_INT m, MKL_INT n, const double* A, MKL_INT lda)
  {
    double out=0, *work = NULL;
    if(norm == 'i' || norm == 'I') 
      {
        work = (double*) malloc(m*sizeof(double));
        if(work == NULL) exit(EXIT_FAILURE);
      }
    out = dlange(&norm, &m, &n, A, &lda, work);
    if(work != NULL) free(work);
    return out;
  }

  template<> single norm
  (char norm, MKL_INT m, MKL_INT n, const scmplx* A, MKL_INT lda)
  {
    single out, *work = NULL;
    if(norm == 'i' || norm == 'I') 
      {
        work = (single*) malloc(m*sizeof(single));
        if(work == NULL) exit(EXIT_FAILURE);
      }
    out = clange(&norm, &m, &n, A, &lda, work);    
    if(work != NULL) free(work);
    return out;
  }

  template<> double norm
  (char norm, MKL_INT m, MKL_INT n, const dcmplx* A, MKL_INT lda)
  {
    double out, *work = NULL;
    if(norm == 'i' || norm == 'I') 
      {
        work = (double*) malloc(m*sizeof(dcmplx));
        if(work == NULL) exit(EXIT_FAILURE);
      }
    out = zlange(&norm, &m, &n, A, &lda, work);
    if(work != NULL) free(work);
    return out;
  }

  template<> void geqrf
  (MKL_INT m, MKL_INT n, single* A, MKL_INT lda, 
   single* tau, single* work, MKL_INT lwork, MKL_INT* info)
  { sgeqrf(&m, &n, A, &lda, tau, work, &lwork, info); }

  template<> void geqrf
  (MKL_INT m, MKL_INT n, double* A, MKL_INT lda, 
   double* tau, double* work, MKL_INT lwork, MKL_INT* info)
  { dgeqrf(&m, &n, A, &lda, tau, work, &lwork, info); }

  template<> void geqrf
  (MKL_INT m, MKL_INT n, scmplx* A, MKL_INT lda, 
   scmplx* tau, scmplx* work, MKL_INT lwork, MKL_INT* info)
  { cgeqrf(&m, &n, A, &lda, tau, work, &lwork, info); }

  template<> void geqrf
  (MKL_INT m, MKL_INT n, dcmplx* A, MKL_INT lda, 
   dcmplx* tau, dcmplx* work, MKL_INT lwork, MKL_INT* info)
  { zgeqrf(&m, &n, A, &lda, tau, work, &lwork, info); }

  
  template<> void qmqr
  (char side, char trans, MKL_INT m, MKL_INT n, MKL_INT k, 
   const single* A, MKL_INT lda, const single* tau, single* C, MKL_INT ldc,
   single* work, MKL_INT lwork, MKL_INT* info)
  { sormqr(&side, &trans, &m, &n, &k, A, &lda, tau, 
           C, &ldc, work, &lwork, info); }

  template<> void qmqr
  (char side, char trans, MKL_INT m, MKL_INT n, MKL_INT k, 
   const double* A, MKL_INT lda, const double* tau, double* C, MKL_INT ldc,
   double* work, MKL_INT lwork, MKL_INT* info)
  { dormqr(&side, &trans, &m, &n, &k, A, &lda, tau, 
           C, &ldc, work, &lwork, info); }

  template<> void qmqr
  (char side, char trans, MKL_INT m, MKL_INT n, MKL_INT k, 
   const scmplx* A, MKL_INT lda, const scmplx* tau, scmplx* C, MKL_INT ldc, 
   scmplx* work, MKL_INT lwork, MKL_INT* info)
  { cunmqr(&side, &trans, &m, &n, &k, A, &lda, tau, 
           C, &ldc, work, &lwork, info); }

  template<> void qmqr
  (char side, char trans, MKL_INT m, MKL_INT n, MKL_INT k, 
   const dcmplx* A, MKL_INT lda, const dcmplx* tau, dcmplx* C, MKL_INT ldc, 
   dcmplx* work, MKL_INT lwork, MKL_INT* info)
  { zunmqr(&side, &trans, &m, &n, &k, A, &lda, tau, 
           C, &ldc, work, &lwork, info); }
  
  // The following set of functions (gghrd, ggbal, ggbak, hgeqz, tgevc) is
  // used for generalized nonsymmetric eigenvalue problems.
  template<> void gghrd
  (char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   single* A, MKL_INT lda, single* B, MKL_INT ldb, 
   single* Q, MKL_INT ldq, single* Z, MKL_INT ldz, MKL_INT* info)
  { sgghrd(&compq, &compz, &n, &ilo, &ihi, 
           A, &lda, B, &ldb, Q, &ldq, Z, &ldz, info ); }

  template<> void gghrd
  (char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   double* A, MKL_INT lda, double* B, MKL_INT ldb, 
   double* Q, MKL_INT ldq, double* Z, MKL_INT ldz, MKL_INT* info)
  { dgghrd(&compq, &compz, &n, &ilo, &ihi, 
           A, &lda, B, &ldb, Q, &ldq, Z, &ldz, info ); }

  template<> void gghrd
  (char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   scmplx* A, MKL_INT lda, scmplx* B, MKL_INT ldb, 
   scmplx* Q, MKL_INT ldq, scmplx* Z, MKL_INT ldz, MKL_INT* info)
  { cgghrd(&compq, &compz, &n, &ilo, &ihi, 
           A, &lda, B, &ldb, Q, &ldq, Z, &ldz, info ); }

  template<> void gghrd
  (char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   dcmplx* A, MKL_INT lda, dcmplx* B, MKL_INT ldb, 
   dcmplx* Q, MKL_INT ldq, dcmplx* Z, MKL_INT ldz, MKL_INT* info)
  { zgghrd(&compq, &compz, &n, &ilo, &ihi, 
           A, &lda, B, &ldb, Q, &ldq, Z, &ldz, info ); }


  template<> void ggbal
  (char job, MKL_INT n, single* A, MKL_INT lda, single* B, MKL_INT ldb, 
   MKL_INT* ilo, MKL_INT* ihi, single* lscale, single* rscale, 
   single* work, MKL_INT* info)
  { sggbal(&job, &n, A, &lda, B, &ldb, ilo, ihi, lscale, rscale, work, info); }

  template<> void ggbal
  (char job, MKL_INT n, double* A, MKL_INT lda, double* B, MKL_INT ldb,
   MKL_INT* ilo, MKL_INT* ihi, double* lscale, double* rscale, 
   double* work, MKL_INT* info)
  { dggbal(&job, &n, A, &lda, B, &ldb, ilo, ihi, lscale, rscale, work, info); }

  template<> void ggbal
  (char job, MKL_INT n, scmplx* A, MKL_INT lda, scmplx* B, MKL_INT ldb, 
   MKL_INT* ilo, MKL_INT* ihi, single* lscale, single* rscale, 
   single* work, MKL_INT* info)
  { cggbal(&job, &n, A, &lda, B, &ldb, ilo, ihi, lscale, rscale, work, info); }

  template<> void ggbal
  (char job, MKL_INT n, dcmplx* A, MKL_INT lda, dcmplx* B, MKL_INT ldb, 
   MKL_INT* ilo, MKL_INT* ihi, double* lscale, double* rscale, 
   double* work, MKL_INT* info)
  { zggbal(&job, &n, A, &lda, B, &ldb, ilo, ihi, lscale, rscale, work, info); }


  template<> void ggbak
  (char job, char side, MKL_INT n, MKL_INT ilo, MKL_INT ihi,
   single* lscale, single* rscale, MKL_INT m, single* v, MKL_INT ldv,
   MKL_INT* info)
  { sggbak(&job, &side, &n, &ilo, &ihi, lscale, rscale, &m, v, &ldv, info); }

  template<> void ggbak
  (char job, char side, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   double* lscale, double* rscale, MKL_INT m, double* v, MKL_INT ldv,
   MKL_INT* info)
  { dggbak(&job, &side, &n, &ilo, &ihi, lscale, rscale, &m, v, &ldv, info); }

  template<> void ggbak
  (char job, char side, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   single* lscale, single* rscale, MKL_INT m, scmplx* v, MKL_INT ldv, 
   MKL_INT* info)
  { cggbak(&job, &side, &n, &ilo, &ihi, lscale, rscale, &m, v, &ldv, info); }

  template<> void ggbak
  (char job, char side, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   double* lscale, double* rscale, MKL_INT m, dcmplx* v, MKL_INT ldv, 
   MKL_INT* info)
  { zggbak(&job, &side, &n, &ilo, &ihi, lscale, rscale, &m, v, &ldv, info); }


  template<> void hgeqz
  (char job, char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   single* A, MKL_INT lda, single* B, MKL_INT ldb, 
   single* alphar, single* alphai, single* beta, 
   single* Q, MKL_INT ldq, single* Z, MKL_INT ldz, 
   single* work, MKL_INT lwork, single* rwork, MKL_INT* info) // rwork not used
  { shgeqz(&job, &compq, &compz, &n, &ilo, &ihi, A, &lda, B, &ldb, 
           alphar, alphai, beta, Q, &ldq, Z, &ldz,  work, &lwork, info); }

  template<> void hgeqz
  (char job, char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   double* A, MKL_INT lda, double* B, MKL_INT ldb, 
   double* alphar, double* alphai, double* beta, 
   double* Q, MKL_INT ldq, double* Z, MKL_INT ldz, 
   double* work, MKL_INT lwork, double* rwork, MKL_INT* info) // rwork not used
  { dhgeqz(&job, &compq, &compz, &n, &ilo, &ihi, A, &lda, B, &ldb, 
           alphar, alphai, beta, Q, &ldq, Z, &ldz,  work, &lwork, info); }

  template<> void hgeqz
  (char job, char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   scmplx* A, MKL_INT lda, scmplx* B, MKL_INT ldb, 
   scmplx* alphar, scmplx* alphai, scmplx* beta, // alphai set to zero
   scmplx* Q, MKL_INT ldq, scmplx* Z, MKL_INT ldz, 
   scmplx* work, MKL_INT lwork, single* rwork, MKL_INT* info)
  { for(int j = 0; j < n; ++j) alphai[j] = scmplx(0.0);
    chgeqz(&job, &compq, &compz, &n, &ilo, &ihi, A, &lda, B, &ldb, 
           alphar, beta, Q, &ldq, Z, &ldz,  work, &lwork, rwork, info); }

  template<> void hgeqz
  (char job, char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   dcmplx* A, MKL_INT lda, dcmplx* B, MKL_INT ldb, 
   dcmplx* alphar, dcmplx* alphai, dcmplx* beta, // alphai set to zero
   dcmplx* Q, MKL_INT ldq, dcmplx* Z, MKL_INT ldz, 
   dcmplx* work, MKL_INT lwork, double* rwork, MKL_INT* info)
  { for(int j = 0; j < n; ++j) alphai[j] = dcmplx(0.0);
    zhgeqz(&job, &compq, &compz, &n, &ilo, &ihi, A, &lda, B, &ldb, 
           alphar, beta, Q, &ldq, Z, &ldz,  work, &lwork, rwork, info); }


  template<> void tgevc
  (char side, char howmny, MKL_INT* select, MKL_INT n, 
   single* A,  MKL_INT lda,  single* B,  MKL_INT ldb,
   single* VL, MKL_INT ldvl, single* VR, MKL_INT ldvr, MKL_INT mm, 
   MKL_INT* m, single* work, single* rwork, MKL_INT* info) // rwork not used
  { stgevc(&side, &howmny, select, &n, A,  &lda, B, &ldb,
           VL, &ldvl, VR, &ldvr, &mm, m, work, info); }

  template<> void tgevc
  (char side, char howmny, MKL_INT* select, MKL_INT n, 
   double* A,  MKL_INT lda,  double* B,  MKL_INT ldb, 
   double* VL, MKL_INT ldvl, double* VR, MKL_INT ldvr, MKL_INT mm, 
   MKL_INT* m, double* work, double* rwork, MKL_INT* info) // rwork not used
  { dtgevc(&side, &howmny, select, &n, A,  &lda, B, &ldb,
           VL, &ldvl, VR, &ldvr, &mm, m, work, info); }

  template<> void tgevc
  (char side, char howmny, MKL_INT* select, MKL_INT n, 
   scmplx* A,  MKL_INT lda,  scmplx* B,  MKL_INT ldb, 
   scmplx* VL, MKL_INT ldvl, scmplx* VR, MKL_INT ldvr, MKL_INT mm, 
   MKL_INT* m, scmplx* work, single* rwork, MKL_INT* info)
  { ctgevc(&side, &howmny, select, &n, A,  &lda, B, &ldb,
           VL, &ldvl, VR, &ldvr, &mm, m, work, rwork, info); }
  
  template<> void tgevc
  (char side, char howmny, MKL_INT* select, MKL_INT n, 
   dcmplx* A,  MKL_INT lda,  dcmplx* B,  MKL_INT ldb,
   dcmplx* VL, MKL_INT ldvl, dcmplx* VR, MKL_INT ldvr, MKL_INT mm, 
   MKL_INT* m, dcmplx* work, double* rwork, MKL_INT* info)
  { ztgevc(&side, &howmny, select, &n, A,  &lda, B, &ldb,
           VL, &ldvl, VR, &ldvr, &mm, m, work, rwork, info); }  

  // The routine forms the LU factorization of a general m-by-n matrix A.
  template<> void getrf
  (MKL_INT m, MKL_INT n, single* a, MKL_INT lda, MKL_INT* ipiv, MKL_INT* info)
  { sgetrf(&m, &n, a, &lda, ipiv, info); }

  template<> void getrf
  (MKL_INT m, MKL_INT n, double* a, MKL_INT lda, MKL_INT* ipiv, MKL_INT* info)
  { dgetrf(&m, &n, a, &lda, ipiv, info); }  

  template<> void getrf
  (MKL_INT m, MKL_INT n, scmplx* a, MKL_INT lda, MKL_INT* ipiv, MKL_INT* info)
  { cgetrf(&m, &n, a, &lda, ipiv, info); }

  template<> void getrf
  (MKL_INT m, MKL_INT n, dcmplx* a, MKL_INT lda, MKL_INT* ipiv, MKL_INT* info)
  { zgetrf(&m, &n, a, &lda, ipiv, info); }

  // This routine solves for X a systems of linear equations of the 
  // form op(A) X = B, where op(A) is A, A^T or A^H.
  template<> void getrs
  (char trans, MKL_INT n, MKL_INT nrhs, single* a, MKL_INT lda, MKL_INT* ipiv, 
   single* b, MKL_INT ldb, MKL_INT* info)
  { sgetrs(&trans, &n, &nrhs, a, &lda, ipiv, b, &ldb, info); }
    
  template<> void getrs
  (char trans, MKL_INT n, MKL_INT nrhs, double* a, MKL_INT lda, MKL_INT* ipiv, 
   double* b, MKL_INT ldb, MKL_INT* info)
  { dgetrs(&trans, &n, &nrhs, a, &lda, ipiv, b, &ldb, info); }

  template<> void getrs
  (char trans, MKL_INT n, MKL_INT nrhs, scmplx* a, MKL_INT lda, MKL_INT* ipiv, 
   scmplx* b, MKL_INT ldb, MKL_INT* info)
  { cgetrs(&trans, &n, &nrhs, a, &lda, ipiv, b, &ldb, info); }

  template<> void getrs
  (char trans, MKL_INT n, MKL_INT nrhs, dcmplx* a, MKL_INT lda, MKL_INT* ipiv, 
   dcmplx* b, MKL_INT ldb, MKL_INT* info)
  { zgetrs(&trans, &n, &nrhs, a, &lda, ipiv, b, &ldb, info); }

  // Computes the LU factorization of a general m-by-n band matrix.
  template<> void gbtrf
  (MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku, 
   single* AB, MKL_INT ldab, MKL_INT* ipiv, MKL_INT* info)
  { sgbtrf(&m, &n, &kl, &ku, AB, &ldab, ipiv, info); }

  template<> void gbtrf
  (MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku, 
   double* AB, MKL_INT ldab, MKL_INT* ipiv, MKL_INT* info)
  { dgbtrf(&m, &n, &kl, &ku, AB, &ldab, ipiv, info); }

  template<> void gbtrf
  (MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku, 
   scmplx* AB, MKL_INT ldab, MKL_INT* ipiv, MKL_INT* info)
  { cgbtrf(&m, &n, &kl, &ku, AB, &ldab, ipiv, info); }

  template<> void gbtrf
  (MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku, 
   dcmplx* AB, MKL_INT ldab, MKL_INT* ipiv, MKL_INT* info)
  { zgbtrf(&m, &n, &kl, &ku, AB, &ldab, ipiv, info); }
  
  // Solves a system of linear equations with an LU-factored band 
  // coefficient matrix, with multiple right-hand sides.
  template<> void gbtrs
  (char trans, MKL_INT n, MKL_INT kl, MKL_INT ku, MKL_INT nrhs, 
   single* AB, MKL_INT ldab, MKL_INT* ipiv, 
   single* b,  MKL_INT ldb,  MKL_INT* info)
  { sgbtrs(&trans, &n, &kl, &ku, &nrhs, AB, &ldab, ipiv, b, &ldb, info); }

  template<> void gbtrs
  (char trans, MKL_INT n, MKL_INT kl, MKL_INT ku, MKL_INT nrhs, 
   double* AB, MKL_INT ldab, MKL_INT* ipiv, 
   double* b,  MKL_INT ldb,  MKL_INT* info)
  { dgbtrs(&trans, &n, &kl, &ku, &nrhs, AB, &ldab, ipiv, b, &ldb, info); }

  template<> void gbtrs
  (char trans, MKL_INT n, MKL_INT kl, MKL_INT ku, MKL_INT nrhs, 
   scmplx* AB, MKL_INT ldab, MKL_INT* ipiv, 
   scmplx* b,  MKL_INT ldb,  MKL_INT* info)
  { cgbtrs(&trans, &n, &kl, &ku, &nrhs, AB, &ldab, ipiv, b, &ldb, info); }

  template<> void gbtrs
  (char trans, MKL_INT n, MKL_INT kl, MKL_INT ku, MKL_INT nrhs, 
   dcmplx* AB, MKL_INT ldab, MKL_INT* ipiv, 
   dcmplx* b,  MKL_INT ldb,  MKL_INT* info)
  { zgbtrs(&trans, &n, &kl, &ku, &nrhs, AB, &ldab, ipiv, b, &ldb, info); }

  // Computes the Cholesky factorization of a symmetric (Hermitian) 
  // positive-definite band matrix.
  template<> void pbtrf
  (char uplo, MKL_INT n, MKL_INT kd, single* AB, MKL_INT ldab, MKL_INT* info)
  { spbtrf(&uplo, &n, &kd, AB, &ldab, info); }

  template<> void pbtrf
  (char uplo, MKL_INT n, MKL_INT kd, double* AB, MKL_INT ldab, MKL_INT* info)
  { dpbtrf(&uplo, &n, &kd, AB, &ldab, info); }

  template<> void pbtrf
  (char uplo, MKL_INT n, MKL_INT kd, scmplx* AB, MKL_INT ldab, MKL_INT* info)
  { cpbtrf(&uplo, &n, &kd, AB, &ldab, info); }

  template<> void pbtrf
  (char uplo, MKL_INT n, MKL_INT kd, dcmplx* AB, MKL_INT ldab, MKL_INT* info)
  { zpbtrf(&uplo, &n, &kd, AB, &ldab, info); }

  // Solves a system of linear equations with a Cholesky-factored symmetric 
  // (Hermitian) positive-definite band coefficient matrix.
  template<> void pbtrs 
  (char uplo, MKL_INT n, MKL_INT kd, MKL_INT nrhs, 
   single* AB, MKL_INT ldab, single* b,  MKL_INT ldb, MKL_INT* info)
  { spbtrs(&uplo, &n, &kd, &nrhs, AB, &ldab, b, &ldb, info); }

  template<> void pbtrs 
  (char uplo, MKL_INT n, MKL_INT kd, MKL_INT nrhs, 
   double* AB, MKL_INT ldab, double* b,  MKL_INT ldb, MKL_INT* info)
  { dpbtrs(&uplo, &n, &kd, &nrhs, AB, &ldab, b, &ldb, info); }

  template<> void pbtrs 
  (char uplo, MKL_INT n, MKL_INT kd, MKL_INT nrhs, 
   scmplx* AB, MKL_INT ldab, scmplx* b,  MKL_INT ldb, MKL_INT* info)
  { cpbtrs(&uplo, &n, &kd, &nrhs, AB, &ldab, b, &ldb, info); }

  template<> void pbtrs 
  (char uplo, MKL_INT n, MKL_INT kd, MKL_INT nrhs, 
   dcmplx* AB, MKL_INT ldab, dcmplx* b,  MKL_INT ldb, MKL_INT* info)
  { zpbtrs(&uplo, &n, &kd, &nrhs, AB, &ldab, b, &ldb, info); }

} // namespace RKS
