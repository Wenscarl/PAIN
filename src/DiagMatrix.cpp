// This file is part of RAIN, released under the terms of the MIT license.

#include "DiagMatrix.hpp"

namespace RKS
{ 
  template<typename F>
  void DiagMatrix<F>::multiply(F eta, F rho, F* in, F* out)
  {
    for(int j = 0; j < this->size(); j++)
      out[j] = (rho*this->A[j]-eta)*in[j];
    return;
  }
  
  template<typename F>
  void DiagMatrix<F>::solve(F mu, F nu, F* in, F* out)
  {
    for(int j = 0; j < this->size(); j++)
      out[j] = in[j]/(nu*this->A[j]-mu);
    return;
  }

  // Explicit instantiations.  
  template class DiagMatrix<single>;
  template class DiagMatrix<double>;
  template class DiagMatrix<scmplx>;
  template class DiagMatrix<dcmplx>;

} // namespace RKS
