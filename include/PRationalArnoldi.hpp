// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_PRATIONALARNOLDI_HEADER
#define RKS_PRATIONALARNOLDI_HEADER

#include <string>
#include <string.h>

#include <mpi.h>

#include "Types.hpp"
#include "AbstractPencil.hpp"
#include "BlasWrapper.hpp"
#include "LapackWrapper.hpp"
#include "ContinuationPair.hpp"
#include "InnerProduct.hpp"
#include "InternalData.hpp"
#include "SeqTemplates.hpp"
#include "ParTemplates.hpp"
#include "RationalArnoldi.hpp"

namespace RKS
{
  // MPI variant of the parallel rational Arnoldi algorithm.
  template <typename F>
  void 
  p_rational_arnoldi 
  (AbstractPencil<F>& pencil, 
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, 
   F* xi, int m, 
   MPI_Comm comm, 
   int* param, 
   int j = 0, 
   InnerProduct<F>*   inner_product  = NULL,
   AbstractPencil<F>* inexact_pencil = NULL, 
   InternalData<F>*   data           = NULL);
  
} // namespace RKS

#endif // RKS_PRATIONALARNOLDI_HEADER
