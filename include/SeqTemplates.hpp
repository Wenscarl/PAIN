// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_SEQTEMPLATES_HEADER
#define RKS_SEQTEMPLATES_HEADER

#include "Types.hpp"

namespace RKS
{  
  // Returns absolute value of z.
  template <typename F>
  typename BaseType<F>::type absolute(F z);

  // Returns 'C' or 'T' for transposing.
  template<typename F> char transpose();

} // namespace RKS

#endif // RKS_SEQTEMPLATES_HEADER
