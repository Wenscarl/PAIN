// This file is part of RAIN, released under the terms of the MIT license.

#ifndef DIAGMATRIX_HEADER
#define DIAGMATRIX_HEADER

#include "Types.hpp"
#include "AbstractPencil.hpp"

namespace RKS
{
  // AbstractPencil instance representing a diagonal matrix. 
  template<typename F>
  class DiagMatrix : public AbstractPencil<F>
  {
  public:
    void multiply(F eta, F rho, F* in, F* out);
    void solve(F mu, F nu, F* in, F* out);
    int size() const {return this->N;};
    
    DiagMatrix(int N, F* A) : N(N), A(A) {};
    ~DiagMatrix() {};
    
  private:
    int N;
    F* A;
  };  

} // namespace RKS

#endif // DIAGMATRIX_HEADER
