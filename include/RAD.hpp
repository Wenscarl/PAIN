// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_RAD_HEADER
#define RKS_RAD_HEADER

#include <string>
#include <string.h>

#include "Types.hpp"
#include "AbstractPencil.hpp"
#include "BlasWrapper.hpp"
#include "LapackWrapper.hpp"
//#include "ContinuationPair.hpp"
#include "InnerProduct.hpp"
//#include "Utils.hpp"

namespace RKS
{
  // For the RAD A V K = B V H, computes the relative residual 
  // || A V K - B V H ||_F / ( ||K||_F + || H ||_F ).
  template <typename F>
  typename BaseType<F>::type rad_residual
  (AbstractPencil<F>& Pencil, 
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, int m);

  // For the RAD A V K = B V H, computes the departure from orthonormality 
  // || I - inner_product(V, V) ||_F.
  template <typename F>
  typename BaseType<F>::type orthonormality
  (int N, int m, F* V, int ldv, 
   InnerProduct<F>* inner_product = new InnerProduct<F>());

} // namespace RKS

#endif // RKS_RAD_HEADER
