// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_RATIONALARNOLDI_HEADER
#define RKS_RATIONALARNOLDI_HEADER

#include <string>
#include <string.h>

#include <omp.h>

#include "Types.hpp"
#include "AbstractPencil.hpp"
#include "BlasWrapper.hpp"
#include "LapackWrapper.hpp"
#include "ContinuationPair.hpp"
#include "InnerProduct.hpp"
#include "InternalData.hpp"
#include "SeqTemplates.hpp"

#include <algorithm>

//#include "Utils.hpp"

namespace RKS
{
  // Sequntial variant of the rational Arnoldi algorithm.
  template <typename F>
  void 
  rational_arnoldi 
  (AbstractPencil<F>& pencil, 
   F* V, int ldv, 
   F* K, int ldk, 
   F* H, int ldh, 
   F* xi, int m, 
   int* param,
   int j = 0, 
   InnerProduct<F>*   inner_product  = NULL,
   AbstractPencil<F>* inexact_pencil = NULL, 
   InternalData<F>*   data           = NULL);

  void   rational_arnoldi_init_param(int* param);
  void polynomial_arnoldi_init_param(int* param);
  
} // namespace RKS

#endif // RKS_RATIONALARNOLDI_HEADER
