// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_TYPES_HEADER
#define RKS_TYPES_HEADER

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <complex>

#define MKL_Complex8  std::complex<float>
#define MKL_Complex16 std::complex<double>

typedef float single;
typedef std::complex<double> dcmplx;
typedef std::complex<single> scmplx;

template<typename Real> 
struct BaseType                       {typedef Real type;};
template<typename Real> 
struct BaseType< std::complex<Real> > {typedef Real type;};
// template<typename F> using Base = typename BaseType<F>::type;

namespace RKS
{
  enum _Param    { Default          = 0, // If set to zero, parameters will be
                   Strategy         = 1, // initialized to default values.
                   Orth             = 2, 
                   Reorth           = 3, 
                   ContinuationRoot = 4,
                   BreakdownTol     = 5,
                   BreakdownIter    = 6, // Output parameter.
                   NearOptStrategy  = 21, 
                   NearOptOrth      = 22,
                   NearOptReorth    = 23,
                   NearOptRoot      = 24 };

  enum _Strategy { Ruhe        = 0, 
                   Last        = 1, 
                   NearOptimal = 2};

  enum _Orth     { MGS = 0, 
                   CGS = 1};

  enum _ContinuationRoot { RootZero         = 0, 
                           RootInfinity     = 1, 
                           RootZeroInfinity = 2, 
                           RootAdaptive     = 3}; // Not suppoerted yet.

  enum _Constants { ArnoldiParamSize = 32 };  

} // namespace RKS

#define FILE_LINE_STR(STR) printf("FILE = %s; LINE = %d; %s", \
                                  __FILE__, __LINE__, (STR));
#define FILE_LINE_INT(INT) printf("FILE = %s; LINE = %d; %d", \
                                  __FILE__, __LINE__, (INT));
#define EXIT_FL {                                               \
    printf("FILE = %s; LINE = %d",  __FILE__, __LINE__);	\
    exit(EXIT_FAILURE);						\
  }

#define EXIT_STR(STR) { FILE_LINE_STR((STR)); exit(EXIT_FAILURE); }
#define EXIT_INT(INT) { FILE_LINE_INT((INT)); exit(EXIT_FAILURE); }

#define EXIT_INFO(STR, INT) {                                           \
    printf("FILE = %s; LINE = %d; routine %s returns info = %d.",       \
           __FILE__, __LINE__, (STR), (INT));                           \
    exit(EXIT_FAILURE); } 
   
#define MPI_FILE_LINE {                                 \
    printf("RKS_MPI_ERR(%s, %d)", __FILE__, __LINE__);  \
    MPI_Finalize();                                     \
  }

#endif //RKS_TYPES_HEADER
