// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_BLASWRAPPER_HEADER
#define RKS_BLASWRAPPER_HEADER

#include "Types.hpp"
#include <mkl.h>


namespace RKS
{     
  template <typename F> F dot
  (MKL_INT n, const F* x, MKL_INT incx, const F* y, MKL_INT incy);

  template <typename F> void scal
  (MKL_INT n, F alpha, F* x, MKL_INT incx);
  
  template <typename F> void axpy
  (MKL_INT n, F alpha, const F* x, MKL_INT incx, F beta, F* y, MKL_INT incy);
  
  template <typename F> void gemv
  (char trans, MKL_INT m, MKL_INT n, F alpha, const F* A, MKL_INT lda,
   /**/                                       const F* x, MKL_INT incx, 
   /**/                              F beta,        F* y, MKL_INT incy);

  template <typename F> void gemm
  (char transA, char transB, MKL_INT m, MKL_INT n, MKL_INT k, 
   F alpha, const F* A, MKL_INT lda,
   /**/     const F* B, MKL_INT ldb,
   F beta,        F* C, MKL_INT ldc);

  template <typename F> void gbmv
  (char trans, MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku,
   F alpha, const F* A, MKL_INT lda, 
   /**/     const F* x, MKL_INT incx,
   F beta,        F* y, MKL_INT incy);

  template <typename F> void hbmv
  (char uplo, MKL_INT n, MKL_INT k,  
   F alpha, const F* A, MKL_INT lda,
   /**/     const F* x, MKL_INT incx,
   F beta,        F* y, MKL_INT incy);
  
} // namespace RKS

#endif // RKS_BLASWRAPPER_HEADER
