// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_RKS_HEADER
#define RKS_RKS_HEADER

#include "Types.hpp"

#include <iostream>
#include <getopt.h>
#include <string>
#include <omp.h>

#include "BlasWrapper.hpp"
#include "LapackWrapper.hpp"
#include "SparseBlasWrapper.hpp"

#include "Utils.hpp"

#include "AbstractPencil.hpp"
#include "InternalData.hpp"
#include "InnerProduct.hpp"

#include "FOMPencil.hpp" 
#include "MKLCSRPencilFD.hpp"

#include "RationalArnoldi.h"
#include "RationalArnoldi.hpp"
#include "RAD.hpp"

#endif // RKS_RKS_HEADER
