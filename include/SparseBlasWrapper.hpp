// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_SPARSEBLASWRAPPER_HEADER
#define RKS_SPARSEBLASWRAPPER_HEADER

#include "Types.hpp"
#include <mkl_spblas.h>


namespace RKS
{
  // Performs y:= alpha*A*x + beta*y.
  template <typename F> void mkl_csrmv
  (char trans, int m, int k, F alpha, char matdescra[6], 
   F* val, int* col, int* ptrb, int* ptre, 
   F* x, F beta, F* y);

  // Performs y := alpha*inv(A)*x.
  template <typename F> void mkl_csrsv
  (char trans, int m, F alpha, char matdescra[6], 
   F* val, int* col, int* ptrb, int* ptre,
   F* x, F* y);

  // Perform C := A+beta*op(B).
  template <typename F> void mkl_csradd
  (char trans, int request, int sort, int m, int n, 
   F* a, int* ja, int* ia, F beta, 
   F* b, int* jb, int* ib, 
   F* c, int* jc, int* ic, int nzmax, int* info);

} // namespace RKS

#endif // RKS_SPARSEBLASWRAPPER_HEADER
