// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_FOMPENCIL_HEADER
#define RKS_FOMPENCIL_HEADER

#include "Types.hpp"
#include "AbstractPencil.hpp"
#include "BlasWrapper.hpp"
#include "LapackWrapper.hpp"
#include "InnerProduct.hpp"
#include "Utils.hpp"
#include "RationalArnoldi.hpp"

#include <limits>

namespace RKS
{  
  // AbstractPencil instance representing the pencil (C, I),
  // where C = nu*A - mu*B, is given by mu, nu and the *Pencil = (A, B).
  // The function solve is essentially not implemented. This is supposed to be 
  // used with FOMPencil for generating a polynomial Krylov space with the 
  // matrix C.
  template<typename F>
  class ShiftedPencil : public AbstractPencil<F>
  {
  public:
    void multiply(F eta, F rho, F* in, F* out);
    void solve(F mu, F nu, F* in, F* out);
    int size() const;
    
    ShiftedPencil(AbstractPencil<F>* Pencil, F mu, F nu);

  private:
    AbstractPencil<F>* Pencil;
    F mu, nu;
  }; // ShiftedPencil<F>

  // AbstractPencil instance where the multiply function carries over
  // from a given instance of type AbstractPencil<F>*, and solve is
  // implemented using the FOM approximation of order m, via a call
  // to rational_arnoldi.
  template<typename F>
  class FOMPencil : public AbstractPencil<F>
  {
  public:
    void multiply(F eta, F rho, F* in, F* out);
    void solve(F mu, F nu, F* in, F* out);
    int size() const;
    
    FOMPencil(AbstractPencil<F>* Pencil, 
              InnerProduct<F>*   inner_product,
              int m, int* _param);
    ~FOMPencil();
    
  private:
    AbstractPencil<F>* Pencil;
    InnerProduct<F>*   inner_product;
    
    F* V; int ldv;
    F* K; int ldk;
    F* H; int ldh;
    F* xi;
    int m;
    int param[ArnoldiParamSize];
  }; // FOMPencil<F>
  
} // namespace RKS
 
#endif // RKS_FOMPENCIL_HEADER
