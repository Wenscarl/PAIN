// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_ABSTRACTPENCIL_HEADER
#define RKS_ABSTRACTPENCIL_HEADER

#include "Types.hpp"

namespace RKS
{
  /* The abstract class AbstractPencil represents an interface 
   * for a matrix pencil (A, B). 
   *
   * It requires the definition of three function, namely
   * - int size() const, which return the size N of the pencil;
   * - void multiply(F eta, F rho, F *in, F *out), which for 
   *   the input vector x=*in of size N-by-1, returns 
   *   *out = (rho*A*x-eta*B*x);
   * - void solve(F mu, F nu, F *in, F *out), which for the input 
   *   vector x=*in of size N-by-1, returns *out = (nu*A-mu*B)\x.
   */
  template <typename F>
  class AbstractPencil
  {
  public:
    virtual void multiply(F eta, F rho, F *in, F *out) = 0;
    virtual void solve(F mu, F nu, F *in, F *out)      = 0;
    virtual int size() const = 0;
    
    virtual ~AbstractPencil(){};  
  };
  
} // namespace RKS

#endif // RKS_ABSTRACTPENCIL_HEADER
