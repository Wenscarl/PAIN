// This file is part of RAIN, released under the terms of the MIT license.

#ifndef MKLCSRPENCILFD_HEADER
#define MKLCSRPENCILFD_HEADER

#include <string.h>
#include <iostream>
#include "Types.hpp"
#include "AbstractPencil.hpp"
#include "BlasWrapper.hpp"
#include "SparseBlasWrapper.hpp"

#include "dmumps_c.h"
#include "zmumps_c.h"

#include "mpi.h"

using std::string;
using std::cout;
using std::endl;

namespace RKS
{     
    template<class T> class MKLCSRPencilFD;    // forward declaration
    
    template<class S>
    void factorMumps(MKLCSRPencilFD<S>& pencil);
    
    template<class S>
    void solveMumps(MKLCSRPencilFD<S>& pencil, S *in, S *out);
  // Pardiso's matrix types.
  enum _mtype { RealSymm         = -2,  // Entire diagonal needs to be stored.
                RealSymmPD       =  2,  // Entire diagonal needs to be stored.
                RealNonSymm      = 11,
                RealStrucSymm    =  1,  // Entire diagonal needs to be stored.
                ComplexSymm      =  6,  // Entire diagonal needs to be stored.
                ComplexHerm      = -4,  // Entire diagonal needs to be stored.
                ComplexHermPD    =  4,  // Entire diagonal needs to be stored.
                ComplexNonSymm   = 13, 
                ComplexStrucSymm =  3}; // Entire diagonal needs to be stored.

  // Instance of AbstractPencil representing a general sparse pencil (A, B)
  // built on top of MKL's Sparse BLAS and PARDISO. The matrices A and B are 
  // represented in CSR format with 1-based indexing. For each row, stored 
  // matrix values have to be ordered increasingly by column index.
  template<typename F>
  class MKLCSRPencilFD : public AbstractPencil<F>
  {
  public:
    // Minimal interface to be satisfied for AbstractPencil.
    void multiply(F eta, F rho, F* in, F* out);
    void solve(F mu, F nu, F* in, F* out);
    void pardisoSolve(F mu, F nu, F* in, F* out);
    int  size() const;
    
    // MUMPS solve and factor
//     void runMumps(F eta, F rho, F* in, F *out); 
//     void factorMumps();
//     void solveMumps(F *in, F* out);
    
     void runMumps(F mu, F nu, F *in, F *out);
    
    template<class S>
    friend void factorMumps(MKLCSRPencilFD<S>& pencil);
    
    template<class S>
    friend void solveMumps(MKLCSRPencilFD<S>& pencil, S *in, S *out);
        
    void MUMPSmemfree();
    void MUMPSmemloc();

    // The following routines provide pointer access to internal data 
    // representing A and/or B; the descriptor gets copied.
    void get_a
    (F* val, int* col, int* row) const;

    void get_a
    (F* val, int* col, int* row, char descr[6]) const;

    void get_b
    (F* val, int* col, int* row) const;

    void get_b
    (F* val, int* col, int* row, char descr[6]) const;

    void set_a
    (F* val, int* col, int* row, char descr[6]);

    void set_b
    (F* val, int* col, int* row, char descr[6]);

    // Return number of elements stored in A or B, respectively.
    int nnza() const;
    int nnzb() const;

    // Allows to modify the internal parameter array for PARDISO.
    // The variable index is 1-based.
    int& operator[] (int index);

    // Public constructors allow to represent the pencils (A, I), (I, B) 
    // or (A, B), where I is the identity matrix of appropriate size.
    MKLCSRPencilFD(int _N, int _mt, 
                   F* val, int* col, int* row, char descr[6],bool _useMUMPS = false);
  
    MKLCSRPencilFD(int _N, int _mt, 
                   F* val, int* col, int* row, char descr[6],
                   int _iparm[64]);

    MKLCSRPencilFD(F* val, int* col, int* row, char descr[6],
                   int _N, int _mt);
  
    MKLCSRPencilFD(F* val, int* col, int* row, char descr[6],
                   int _N, int _mt, 
                   int _iparm[64]);
  
    MKLCSRPencilFD(int _N, int _mt,
                   F* val1, int* col1, int* row1, char descr1[6],
                   F* val2, int* col2, int* row2, char descr2[6]);

    MKLCSRPencilFD(int _N, int _mt,
                   F* val1, int* col1, int* row1, char descr1[6],
                   F* val2, int* col2, int* row2, char descr2[6],
                   int _iparm[64]);
  
    ~MKLCSRPencilFD();

  private: 
    void init(int _N, int _mt);
    void set_identity(char mat);
    void allocate_c();
    void form_c(F mu, F nu);  

  private:
    int  N; 

    F*   vala; // length = nnza
    int* cola; // length = nnza
    int* rowa; // length = N+1
    char     descra[6];

    F*   valb; // length = nnzb
    int* colb; // length = nnzb
    int* rowb; // length = N+1
    char     descrb[6]; 

    char    identity;

    F _mu;
    F _nu;
    int _phase;

    // For PARDISO.

    int  max_nnzc;
    F*   valc; // length = nnzc <= nnza + nnzb
    int* colc; // length = nnzc <= nnza + nnzb
    int* rowc; // length = N+1
    int  call_pardiso_cnt;
    int  call_mumps_cnt;

    int mtype;
    void  *pt[64];
    int iparm[64];
    int maxfct, mnum, error, msglvl, nrhs;
    int* perm;
    
    
    // For MUMPS (seq)
    bool isMUMPS;
    int umfn;
    int	umfnz;
    DMUMPS_STRUC_C DMUMPS;
    ZMUMPS_STRUC_C ZMUMPS;

    int *Ar;
    int *Ac;
    
    DMUMPS_REAL *DB;
    DMUMPS_REAL *DAmumps;
    
    ZMUMPS_COMPLEX *ZAmumps;
    ZMUMPS_COMPLEX *ZB;// for backup use
  };
  
 template<> void solveMumps<dcmplx>(MKLCSRPencilFD<dcmplx>& , dcmplx *, dcmplx *);
 template<> void factorMumps<dcmplx>(MKLCSRPencilFD<dcmplx>&);
 
 
} // namespace RKS

#endif // MKLCSRPENCILFD_HEADER
