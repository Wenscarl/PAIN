// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_INNERPRODUCT_HEADER
#define RKS_INNERPRODUCT_HEADER

#include "Types.hpp"
#include "AbstractPencil.hpp"
#include "BlasWrapper.hpp"

namespace RKS
{
  // Functor representing the (standard) Euclidean inner product.
  template <typename F>
  class InnerProduct
  {
  public:  
    // Output C is an m-by-n matrix, the matrices X and Y are of 
    // size k-by-n and k-by-m, respectively.
    virtual void operator()(int m, int n, int k, 
                            const F* X, int ldx, 
                            const F* Y, int ldy,
                            /**/  F* C, int ldc);
  };

  // Functor representing the non-standard inner product given by 
  // inner_product(x, y) = y' * (rho*A-eta*B)*x, where (rho*A-eta*B) is assumed
  // to be positive definite. The pencil (A, B) is represented by Pencil.
  template <typename F>
  class PencilInnerProduct : public InnerProduct<F>
  {
  public:  
    // Public constructor.
    PencilInnerProduct(AbstractPencil<F>* Pencil, F eta, F rho);

    // Public destructor.
    ~PencilInnerProduct(); 

    // Output C is an m-by-n matrix, the matrices X and Y are of 
    // size k-by-n and k-by-m, respectively.
    void operator()(int m, int n, int k, 
                    const F* X, int ldx, 
                    const F* Y, int ldy,
                    /**/  F* C, int ldc);
  private:
    F eta, rho;
    AbstractPencil<F>* Pencil;
    F* Z;
  };
  
} // namespace RKS

#endif // RKS_INNERPRODUCT_HEADER
