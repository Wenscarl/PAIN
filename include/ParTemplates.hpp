// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_PARTEMPLATES_HEADER
#define RKS_PARTEMPLATES_HEADER

#include "mpi.h"
#include "Types.hpp"

namespace RKS
{  
  // Return the MPI_Datatype corresponding to template type F.
  // Implemented for type F being float, double, complex<float> 
  // or complex<double>, at the end of this document.
  template<typename F> MPI_Datatype MPI_GetDataType();

} // namespace RKS

#endif // RKS_PARTEMPLATES_HEADER
