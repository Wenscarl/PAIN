// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_UTILS_HEADER
#define RKS_UTILS_HEADER

#include <string>
#include <iomanip> 
#include <iostream>
#include <fstream>

#include "Types.hpp"

using std::string;
using std::cout;
using std::endl;
namespace RKS
{

    
  // Prints a matrix.
  template <typename F>
  void print_matrix(char* name, int m, int n, const F* A, int lda, 
                    int precision = 4, int w = 12);
} // namespace RKS

#endif //RKS_UTILS_HEADER
 
