// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_CONTINUATIONPAIR_HEADER
#define RKS_CONTINUATIONPAIR_HEADER

#include <string>
#include <string.h>

#include "Types.hpp"
#include "SeqTemplates.hpp"
#include "AbstractPencil.hpp"
#include "InnerProduct.hpp"
#include "BlasWrapper.hpp"
#include "LapackWrapper.hpp"
#include "RationalArnoldi.hpp"
#include "Utils.hpp"

namespace RKS
{    
  // Computes the full QR factorization of the order-by-(order-1) matrix L, 
  // and returns the last column of the Q factor via vt.
  template <typename F>
  void last_column_of_q(int order, F* L, int ldl, F* vt);

  // Computes a (near) optimal continuation pair of given order for the pencil 
  // (H, K) of size order-by-order. The matrices H and X are overwritten. The 
  // produced continuation pair is given by (eta/rho, vt).
  template <typename F>
  void optimal_pair(int order, F mu, F nu, 
                    F* K, F* _K, int ldk, F* H, F* _H, int ldh, 
                    F* eta, F* rho, F* vt);  

  // Computes the generalized eigenvalue decomposition of the pair (H, K) of
  // size order-by-order.
  // For real-valued data the generalized eigenvalues are given by
  // (alphar + 1i*alphai, beta), while the eigenvectors are stored in X if
  // real-valued, otherwise their real and imaginary parts are stored in two
  // consecutive columns of X.
  // For complex-valued data the generalized eigenvalues are given by
  // (alphar, beta), and the corresponding eigenvectors are stored in X.
  template <typename F>
  void pencil_roots(int order, F* K, int ldk, F* H, int ldh, 
                    F* alphar, F* alphai, F* beta, F* X);

  // Class used to produce continuation pairs for/during the construction of a
  // rational Krylov space. Currently supported continuation strategies are
  //    RKS::Ruhe (default), 
  //    RKS::Last, and
  //    RKS::NearOptimal.
  template <typename F>
  class ContinuationPair
  {
  public:
    ContinuationPair
    (AbstractPencil<F>* pencil,
     AbstractPencil<F>* inexact_pencil,
     F* V, int ldv, 
     F* K, int ldk, 
     F* H, int ldh, 
     F* vw, F* vc, int _param[ArnoldiParamSize], int max_order, 
     InnerProduct<F>* inner_product);
    
    void operator()(int order, F xi, 
                    F* mu, F* nu, F* eta, F* rho, F* vt);
    
    ~ContinuationPair();
    
  private:
    AbstractPencil<F>* pencil;
    AbstractPencil<F>* inexact_pencil;    
    
    F* V; int ldv;
    F* K; int ldk;
    F* H; int ldh;
    F* vw; F* vc;
    
    F* L; int ldl; // Ruhe

    F* _K; F* _H;  // NearOptimal
        
    int max_order;
    int param[ArnoldiParamSize];
    
    InnerProduct<F>* inner_product;  
  };  
  
  
} // namespace RKS

#endif // RKS_CONTINUATIONPAIR_HEADER
