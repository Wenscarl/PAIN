// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_INTERNALDATA_HEADER
#define RKS_INTERNALDATA_HEADER

#include <string>
#include <iomanip> 
#include <iostream>
#include <fstream>
#include <string.h>
#include <omp.h>

#include "Types.hpp"

namespace RKS
{
  enum TimeLen { TimeNSeq = 5, 
		 TimeNPar = 6 };

  enum TimeTag { TimeContPair = 0,
		 TimeVt       = 1, 
		 TimeMultiply = 2, 
		 TimeSolve    = 3,
		 TimeOrth     = 4,
		 TimeOrthSync = 5};

  template <typename F>
  class InternalData
  {
  private:
    int ldr, order;
    F *R, *T, *eta, *rho, *mu, *nu;
    double *partial[6];
    double total;
    double(*timer)(void);
  public:
    InternalData(int, double(*)(void));
    ~InternalData();
    void total_bgn();
    void total_end();
    void total_print(int precision = 3, int w = 12);

    void partial_bgn(TimeTag, int);
    void partial_end(TimeTag, int);
    void partial_print(int rows = TimeNSeq, int cols = -1, 
		       int incr = 1,        int precision = 3,
		       int w    = 12);

    // Copies vector vv of size j into the leading j rows 
    // of R's jth column. (For j = 1 into the first one.) 
    void set_column_r(F*, int);

    // Copies vector vv of size j into the leading j rows 
    // of T's jth column. (For j = 1 into the first one.) 
    void set_column_t(F*, int);

    // Copies parameters (eta/rho, mu/nu) into the (j+1)st position of
    // the corresponding vectors. (For j = 0 into the first position.)
    void set_moebius(F, F, F, F, int);

    // Provides copies of the relevant data. (Not timings.)
      void get_data(F*, F*, F*, F*, F*, F*);
  }; // InternalData<F>      
  
} // namespace RKS

#endif // RKS_INTERNALDATA_HEADER
