// This file is part of RAIN, released under the terms of the MIT license.

#ifndef RKS_LAPACKWRAPPER_HEADER
#define RKS_LAPACKWRAPPER_HEADER

#include "Types.hpp"
#include "BlasWrapper.hpp"

namespace RKS
{
  // Computes the 'F', '1' or 'I' norm of an m-by-n matrix A. 
  template <typename F> typename BaseType<F>::type norm
  (char norm, MKL_INT m, MKL_INT n, const F* A, MKL_INT lda);

  template <typename F> void geqrf
  (MKL_INT m, MKL_INT n, F* A, MKL_INT lda, 
   F* tau, F* work, MKL_INT lwork, MKL_INT* info);

  template <typename F> void qmqr
  (char side, char trans, MKL_INT m, MKL_INT n, MKL_INT k, 
   const F* A, MKL_INT lda, const F* tau, F* C, MKL_INT ldc, 
   F* work, MKL_INT lwork, MKL_INT* info);

  // The following set of functions (gghrd, ggbal, ggbak, hgeqz, tgevc) is
  // used for generalized nonsymmetric eigenvalue problems.

  template <typename F> void gghrd
  (char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   F* A, MKL_INT lda, F* B, MKL_INT ldb, 
   F* Q, MKL_INT ldq, F* Z, MKL_INT ldz, MKL_INT* info);
  
  template <typename F> void ggbal
  (char job, MKL_INT n, F* A, MKL_INT lda, F* B, MKL_INT ldb, 
   MKL_INT* ilo, MKL_INT* ihi, 
   typename BaseType<F>::type* lscale, 
   typename BaseType<F>::type* rscale, 
   typename BaseType<F>::type* work,   MKL_INT* info);

  template <typename F> void ggbak
  (char job, char side, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   typename BaseType<F>::type* lscale, 
   typename BaseType<F>::type* rscale, 
   MKL_INT m, F* v, MKL_INT ldv, MKL_INT* info);

  template <typename F> void hgeqz
  (char job, char compq, char compz, MKL_INT n, MKL_INT ilo, MKL_INT ihi, 
   F* A, MKL_INT lda, F* B, MKL_INT ldb, 
   F* alphar, F* alphai, F* beta, // alphai set to zero
   F* Q, MKL_INT ldq, F* Z, MKL_INT ldz, 
   F* work, MKL_INT lwork, typename BaseType<F>::type* rwork, MKL_INT* info);
  
  template <typename F> void tgevc
  (char side, char howmny, MKL_INT* select, MKL_INT n, 
   F* A,  MKL_INT lda,  F* B,  MKL_INT ldb,
   F* VL, MKL_INT ldvl, F* VR, MKL_INT ldvr, MKL_INT mm, 
   MKL_INT* m, F* work, typename BaseType<F>::type* rwork, MKL_INT* info);

  // The routine forms the LU factorization of a general m-by-n matrix A.
  template <typename F> void getrf
  (MKL_INT m, MKL_INT n, F* a, MKL_INT lda, MKL_INT* ipiv, MKL_INT* info);

  // This routine solves for X a systems of linear equations of the 
  // form op(A) X = B, where op(A) is A, A^T or A^H.
  template <typename F> void getrs
  (char trans, MKL_INT n, MKL_INT nrhs, F* a, MKL_INT lda, MKL_INT* ipiv, 
   F* b, MKL_INT ldb, MKL_INT* info);

  // Computes the LU factorization of a general m-by-n band matrix.
  template <typename F> void gbtrf
  (MKL_INT m, MKL_INT n, MKL_INT kl, MKL_INT ku, 
   F* AB, MKL_INT ldab, MKL_INT* ipiv, MKL_INT* info);

  // Solves a system of linear equations with an LU-factored band 
  // coefficient matrix, with multiple right-hand sides.
  template <typename F> void gbtrs
  (char trans, MKL_INT n, MKL_INT kl, MKL_INT ku, MKL_INT nrhs, 
   F* AB, MKL_INT ldab, MKL_INT* ipiv, 
   F* b,  MKL_INT ldb,  MKL_INT* info);

  // Computes the Cholesky factorization of a symmetric (Hermitian) 
  // positive-definite band matrix.
  template <typename F> void pbtrf
  (char uplo, MKL_INT n, MKL_INT kd, F* AB, MKL_INT ldab, MKL_INT* info);

  // Solves a system of linear equations with a Cholesky-factored symmetric 
  // (Hermitian) positive-definite band coefficient matrix.
  template <typename F> void pbtrs 
  (char uplo, MKL_INT n, MKL_INT kd, MKL_INT nrhs, 
   F* AB, MKL_INT ldab, F* b,  MKL_INT ldb, MKL_INT* info);

} // namespace RKS

#endif // RKS_LAPACKWRAPPER
